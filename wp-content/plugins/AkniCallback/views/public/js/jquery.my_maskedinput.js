/* --- MY PHONE MASK ------------------------ */
jQuery.fn.phoneMask = function(){
    var input = this;

    (function($){
	    var NewMasked = {
	        inp: input.attr('id'),
	        valStr: '',
	        value: '',

	        init: function(){
	            this._focus();
	            this._change();
	        },

	        _createValue: function(){
	            var inpVal = String(this.value);
	            return '+' + (inpVal.charAt(0) || '_') + (inpVal.charAt(1) || '_') + ' (' + (inpVal.charAt(2) || '_') + (inpVal.charAt(3) || '_') + (inpVal.charAt(4) || '_') + ') ' + (inpVal.charAt(5) || '_') + (inpVal.charAt(6) || '_') + '-' + (inpVal.charAt(7) || '_') + (inpVal.charAt(8) || '_') + '-' + (inpVal.charAt(9) || '_') + (inpVal.charAt(10) || '_') + (inpVal.charAt(11) || '_');
	        },

	        _focus: function(){
	            var _this = this;
	            $('#' + _this.inp).on('click focus', function(e){
	                var inpValue = $(this).val();

	                $(this).val(_this._createValue());

	                var intInpValue = parseInt(inpValue.replace(/\D+/g,"").slice(3));
	                $('#' + _this.inp + '_hidden').val(intInpValue);
	                
	                _this.setCaretPosition(_this.correctPos(String(_this.value).length));
	            });
	        },

	        _change: function(){
	            var _this = this;
	            $('#' + _this.inp).on('input', function(e){
	                var inpValue = $(this).val();

	                var intInpValue = parseInt(inpValue.replace(/\D+/g,""));

	                if(!isNaN(intInpValue)){
	                    _this.value = intInpValue;

	                    var pos = _this.correctPos(String(_this.value).length);
	                }else{
	                    _this.value = '';

	                    var pos = _this.correctPos(0);
	                }
	                $(this).val(_this._createValue());
	                _this.setCaretPosition(pos);
	                //hard for mobile
	                setTimeout(function(){_this.setCaretPosition(pos);}, 20);
	            });
	        },

	        correctPos: function(pos){
	            switch(true){
	                case pos < 3:
	                    pos += 1;
	                    return pos;
	                case pos <= 5:
	                    pos += 3;
	                    return pos;
	                case pos <= 7:
	                    pos += 5;
	                    return pos;
	                case pos <= 9:
	                    pos += 6;
	                    return pos;
	                case pos > 9:
	                    pos += 7;
	                    return pos;
	            }
	        },

	        setCaretPosition: function(pos){
	            var ctrl = document.getElementById(this.inp);

	            if(ctrl.setSelectionRange)
	            {
	                ctrl.setSelectionRange(pos,pos);
	            }
	            else if (ctrl.createTextRange) {
	                var range = ctrl.createTextRange();
	                range.collapse(true);
	                range.moveEnd('character', pos);
	                range.moveStart('character', pos);
	                range.select();
	            }
	        }
	    }

    NewMasked.init();
    })(jQuery);
}
/* ========================================== */