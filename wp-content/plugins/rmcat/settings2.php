<?php defined( 'ABSPATH' ) OR die();
/**
 * passengers_num
 * baggage_number
 * doors_number
 * transmission
 */
class Settings2
{
    public static function init()
    {
        // FIX: 
        add_filter( "radio_buttons_for_taxonomies_launch", [ 'Settings2', 'filter_taxonomy' ] );
    
    }
    
    /**
     * отключаем преобразование списка metabox checkbox -> radio
     * в плагине radio_buttons_for_taxonomies
     */
    public static function filter_taxonomy( $taxonomy ) 
    {
        if ( ! empty($taxonomy) && 'filter-car' === self::get_post_type() ) {
            return [];
        }
        return $taxonomy;
    }
    

    protected static function get_post_type()
    {
        $post_type = isset($_GET['post_type']) ? esc_attr($_GET['post_type']) : '';
        $post_id   = isset($_GET['post']) ? absint($_GET['post']) : '';

        if ( ! $post_type ) {
            $post_type = get_post_type( $post_id );
        }

        return $post_type;
    }


}

/*
    /wp-content/plugins/radio-buttons-for-taxonomies/radio-buttons-for-taxonomies.php
	public function launch( $taxonomy ){
		if ( isset( $this->options['taxonomies'] ) ) {
			$options_taxonomies = apply_filter('radio_buttons_for_taxonomies_launch', (array) $this->options['taxonomies']);

			if( ! empty( $options_taxonomies ) && in_array( $taxonomy, (array) $options_taxonomies ) ) {
				$this->taxonomies[$taxonomy] = new WordPress_Radio_Taxonomy( $taxonomy );
			}
		}
    }
    
*/