<?php

/* Template Name: 404 */
global $core;

$context = $core->get_context();
$context['breadcrumbs']['0'] = [
    'url' => '',
    'title' => 404
];
wp_head();
wp_footer();
$core->render('404.twig', $context);
