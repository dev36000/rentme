<?php

/* Template Name: Search Template */
use Akni\app\Helper\Data;

global $core;
global $post;

$context = $core->get_context();
$model  = $core->getModel();
$config = $core->getConfig();
$configs = $config->getConfig('show','show');
$lang_config = $config->getConfig('lang');
$context = $core->get_context();
//$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

global $wp_query;

if(empty($_GET['s'])) {
    $context['not_found'] = __($lang_config['empty_search_text']);
} else{
    $search = $wp_query->posts;
    foreach($search as &$val){
        $val->page_url = get_permalink ($val->ID);

    }
    if(!empty($search)){
        $context['search_results'] = $wp_query->posts;
        $context['finded'] = $wp_query->found_posts;
        $context['form_url'] =  esc_url( home_url( '/' ) );
    }
    else{
        $context['not_found'] = sprintf(__($lang_config['results_not_found']), $_GET['s']);
    }
    $context['search_question'] = $_GET['s'];
}

$core->render('search-template.twig', $context);