<?php
/* Template Name: Home Template */

use Akni\app\Helper\Data;
use Akni\app\Model\CarPrice;

global $core;
global $post;

$context = $core->get_context();
$model  = $core->getModel();
$config = $core->getConfig();
$showConfig = $config->getConfig('show', 'show');
$langConfig = $config->getConfig('lang');
$cities = $core->getCities();
$priceHandler = new CarPrice();

if ($post && $post->post_type == 'page') {

    $model->setResult([$post]);
    $model->formattedAcf();
    $context['page'] = $model->getResult()[0];

    #Banners
    $bannerValues =  $context['page']->acf['banner_fields']['value'];
    if (!empty($bannerValues)) {
        $banners = Data::sortFields($bannerValues);
        if (!empty($banners) ) {
            $context['banners'] = $banners;
        }
    }

    #picker
    $context['cities'] = $cities->getCities();
    $context['car_classes'] = get_terms('class', ['hide_empty' => true,]);
    $context['car_counter'] = Data::getPostInTermCounter('car',['cities'=>$context['current_cityID']]);

    #Best Deals
    $bestDeals = [];
    $mainTerm = Data::getMainTerm($context['page']->ID, 'cities');
    $mainTermTitle = get_field('where_text',"cities_{$mainTerm->term_id}");

    if ($showConfig['best_deals']) {
        $model->setArgs($showConfig['best_deals']);
        $model->setSpecialArgs('tax_query', [
            [
                "taxonomy" => "cities",
                "field" => "term_id",
                "terms" => $mainTerm->term_id,
            ]
        ]);

        $model->setPosts();
        $model->setMainThumbnailUrls([300, 300]);
        $model->setPostUrls();
        $model->formattedACF();
        $bestDeals = $model->getResult();

        if (!empty($bestDeals)) {
            foreach($bestDeals as $key=>$deal) {
                $priceHandler->setProduct($deal);

                $bestDeals[$key]->regularPrice = $priceHandler->getRegularPrice();
                $bestDeals[$key]->specialPrice = $priceHandler->getSpecialPrice();
                $bestDeals[$key]->calculatedPrice = $priceHandler->calculatePrice();

                /**
                 * new price table
                 */
                $bestDeals[$key]->priceTable = $priceHandler->getPriceTable();
                $bestDeals[$key]->deliveryPrice = $priceHandler->getDeliveryPrice();
                $dealClass = Data::getMainTerm($deal->ID, 'class');
                if (!$bestDeals[$key]->specialPrice || !$bestDeals[$key]->regularPrice) {
                    unset($bestDeals[$key]);
                    continue;
                }

                if (!$dealClass->name) {
                    unset($bestDeals[$key]);
                    continue;
                }


                $dealCities = get_the_terms($deal->ID, 'cities');
                $orderCitiesArr = [];
                $orderCities = '';
                foreach ($dealCities as $city ){
                    $orderCitiesArr[] = $city->name;
                }

                //Adding  label  to car
                $labels = get_the_terms($deal->ID, 'label');
                $labelsArr = [];

                foreach ($labels as  $label){
                    $label->color_text =  get_field('color_text', $label);
                    $label->color_label =  get_field('color_label', $label);
                    $labelsArr[] = get_object_vars($label);
                }

                $bestDeals[$key]->car_labels = $labelsArr;

                $orderCities = implode(',', $orderCitiesArr);
                $bestDeals[$key]->orderCities = $orderCities;
                $bestDeals[$key]->class_label = $dealClass->name;

            }
        }
        if (!empty($bestDeals)) {
            $context['bestDeals']['title'] = $mainTermTitle;
            $context['bestDeals']['deals'] = $bestDeals;
        }
    }

    #Mini Banners
    $bannerValuesMini =  $context['page']->acf['banner_fields_mini']['value'];
    if (!empty($bannerValuesMini)) {
        $bannersMini = Data::sortFields($bannerValuesMini);
        if (!empty($bannersMini) ) {
            $context['bannersMini'] = $bannersMini;
        }
    }

    #Mini Banners
    $bannerValuesMiniMobile =  $context['page']->acf['banner_fields_mini_mobile']['value'];
    if (!empty($bannerValuesMiniMobile )) {
        $bannersMiniMobile = Data::sortFields($bannerValuesMiniMobile);
        if (!empty($bannersMiniMobile) ) {
            $context['bannersMiniMobile'] = $bannersMiniMobile;
        }
    }

    #meta data
    $context['meta_title'] = get_post_meta($context['page']->ID,'_yoast_wpseo_title', true);
    if ($context['meta_title'] =='') {
        $context['meta_title'] = $context['page']->post_title;
    }

    #Description
    $description = $context['page']->acf['description']['value'];
    if (!empty($description)) {
	$context['description'] = $description;
    }

    #h1 - 20.03.2018
    $h1 = $context['page']->acf['title_h1']['value'];
    if ( ! empty($h1) ) {
        $context['title_h1'] = $h1;
    }
       
}
        //  <span class="days">{{ lang.for_day }}</span>
        if (isset($context['lang']['for_day_png'])) {
            $context['lang']['day_src'] = THEME_URI . '/public/images/' . $context['lang']['for_day_png'] . '.png';
        }  

add_head_footer($context);
$core->render('home-template.twig', $context);