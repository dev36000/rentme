<?php
/* Template Name: Faq Template */
use Akni\app\Helper\Data;

global $core;
global $post;

$context = $core->get_context();
$model  = $core->getModel();
$config = $core->getConfig();

$showConfig = $config->getConfig('show', 'show');

if ($post && $post->post_type == 'page') {
    $context['page'] = $post;
    $model->setResult([$post]);
    $model->formattedAcf();
    $context['page'] = $model->getResult()[0];
    #FAQ
    $faqValues =  $context['page']->acf['faq']['value'];
    if (!empty($faqValues)) {
        $faq = Data::sortFields($faqValues);
        if ( !empty($faq) ) {
            $context['faq'] = $faq;
        }
    }

    #meta data
    $context['meta_description'] = get_post_meta($context['page']->ID,'_yoast_wpseo_metadesc', true);
    $context['meta_title'] = get_post_meta($context['page']->ID,'_yoast_wpseo_title', true);
    if ($context['meta_title'] =='') {
        $context['meta_title'] = $context['page']->post_title;
    }
}

$context['breadcrumbs']['0'] = [
    'url' => $context['page']->post_url,
    'title' => $context['page']->post_title
];

add_head_footer($context);
$core->render('faq.twig', $context);