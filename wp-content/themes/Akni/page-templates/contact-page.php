<?php

/* Template Name: Contact Template */

global $core;
global $post;

$context = $core->get_context();
$model  = $core->getModel();
$config = $core->getConfig();

if ($post && $post->post_type == 'page') {
    $model->setResult(['0'=>$post]);
    $model->formattedACF();
    $post = $model->getResult();
    $context['page'] = $post[0];
    $context['breadcrumbs']['0'] = [
        'url' => get_permalink($context['page']->ID),
        'title' =>$context['page']->post_title
    ];

    #meta data
    $context['meta_description'] = get_post_meta($context['page']->ID,'_yoast_wpseo_metadesc', true);
    $context['meta_title'] = get_post_meta($context['page']->ID,'_yoast_wpseo_title', true);
    if ($context['meta_title'] =='') {
        $context['meta_title'] = $context['page']->post_title;
    }
    if ($phone = $context['page']->acf['phone']['value']) {
        $phone = preg_replace('/[^\d+]/', '', $phone);
        $context['page']->tel = $phone;
    }
    if ($viber = $context['page']->acf['viber']['value']) {
        $viber = preg_replace('/[^\d+]/', '', $viber);
        $context['page']->viber = $viber;
    }
}

add_head_footer($context);
$core->render('contact-template.twig', $context);