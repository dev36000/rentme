<?php

use Akni\app\Helper\Data;
use Akni\app\Model\CarPrice;

global $core;
global $post;

$context = $core->get_context();
$model  = $core->getModel();
$config = $core->getConfig();
$showConfig = $config->getConfig('show', 'show');

$breadcrumbs = [];

if ($post) {
    $context['car'] = $post;
    $model->setResult([$post]);
    $model->setMainThumbnailUrls();
    $model->setPostUrls();
    $model->formattedAcf();
    $context['car'] = $model->getResult()[0];
    $context['car']->mini_thumb = get_the_post_thumbnail_url($context['car']->ID, [150, 150]);
    $priceHandler = new CarPrice();

    #meta data
    $context['meta_description'] = get_post_meta($context['car']->ID,'_yoast_wpseo_metadesc', true);
    $context['meta_title'] = get_post_meta($context['car']->ID,'_yoast_wpseo_title', true);
    if ($context['meta_title'] =='') {
        $context['meta_title'] = $context['car']->post_title;
    }

    /*28.02.2019 most popular cer*/
    $context['set_post_views'] = $model->set_post_views($context['car']->ID);
    $context['get_post_views'] = $model->get_post_views($context['car']->ID);
}


$mainCity = Data::getMainTerm($context['car']->ID, 'cities');
$mainClass = Data::getMainTerm($context['car']->ID, 'class');


$allCity = Data::getAllTerm($context['car']->ID, 'cities');
$currentCity = $context['city_info']['name'] ;

$setCity = [];
foreach ( $allCity  as $city){
    if  ($city->name  ==  $currentCity){
        $setCity  =  $city;
    }
}

#breadcrumbs
if($setCity->term_id !='') {
    $mainTermTitle = get_field('where_text',"cities_{$setCity->term_id}");
    $city = $core->getCities()->getCityById($setCity->term_id);
    $pageTitle = $mainTermTitle ? $mainTermTitle : $city->name;
    if ($city->catalogID !='') {
        $context['breadcrumbs']['0'] = [
            'url' => get_permalink($city->catalogID),
            'title' => $context['lang']['cars_text'] . ' ' .  $pageTitle
        ];
    }
}
$context['breadcrumbs']['1'] = [
    'url' => $context['car']->post_url,
    'title' =>$context['car']->post_title
];

#car
$priceHandler->setProduct($context['car']);

$context['car']->regularPrice = $priceHandler->getRegularPrice();
$context['car']->specialPrice = $priceHandler->getSpecialPrice();
$context['car']->class_label = $mainClass->name;
$context['car']->calculatedPrice = $priceHandler->calculatePrice();
$context['car']->deliveryPrice = $priceHandler->getDeliveryPrice();
/**
 * new price table
 */
$context['car']->priceTable = $priceHandler->getPriceTable();
$carClass = Data::getMainTerm($context['car']->ID, 'class');

$carCities = get_the_terms($context['car']->ID, 'cities');
$orderCitiesArr = [];
$orderCities = '';

foreach ($carCities as $city ){
    $orderCitiesArr[] = $city->name;
}
$orderCities = implode(',', $orderCitiesArr);
$context['car']->orderCities = $orderCities;


#advanced attributes
$advancedAttributes = [];

if (!empty($context['car']->acf)) {
    foreach ($context['car']->acf as $key=>$value) {
        if (Data::isStartFrom('attribute_',$key)) {
           if ($value['value'] != '') {
               $advancedAttributes[$key]['label'] = __($value['label']);
               $advancedAttributes[$key]['type'] = $value['type'];
               $advancedAttributes[$key]['value'] = $value['value'];
           }
        }
    }
}

if (!empty($advancedAttributes)) {
    $context['advancedAttributes'] = $advancedAttributes;
}


#add_options_auto
/*
[value] => Array
        (
            [0] => Array
                (
                    [id] => gps
                    [name_opt] => GPS
                    [price] => 5
                )

action	sendCallback
data	type=order&info=%D0%9F%D1%80%D0%BE%D0%BA%D0%B0%D1%82%20Skoda%20Octavia%20A5&
sum=%24100
&days=2
&name=sdfdf%20sdfdsgfdfs
&phone=%2B22%20(222)%2022-22-222
&city=%D0%9A%D0%B8%D0%B5%D0%B2
&date_from=17.04.2018
&time_from=17.04.2018%2012%3A00
&time_to=19.04.2018%2012%3A00
&time_from=17.04.2018%2012%3A00
&date_to=19.04.2018&time_to=19.04.2018%2012%3A00
&delivery=&gps=&sets=

*/
if (!empty($context['car']->acf) && isset($context['car']->acf['add_options_auto'])
&& is_array($context['car']->acf['add_options_auto']['value']) ) {
    $context['adv_options_auto'] = $context['car']->acf['add_options_auto']['value'];
}

#Similar cars
$similarCars = [];
$similarCars = $context['car']->acf['similar_cars']['value'];
if (!empty($similarCars)) {
    foreach ($similarCars as $k=>$v) {
        if ($v->ID == $context['car']->ID) {
            unset($similarCars[$k]);
        }
    }
}
if (!empty($similarCars)) {
    $model->setResult( $similarCars);
    $model->setMainThumbnailUrls();
    $model->setPostUrls();
    $model->formattedAcf();
    $similarCars = $model->getResult();
    foreach($similarCars as $key=>$deal) {
        if ($deal->post_status !='publish') {
            unset($similarCars[$key]);
            continue;
        }
        $priceHandler->setProduct($deal);
        $similarCars[$key]->regularPrice = $priceHandler->getRegularPrice();
        $similarCars[$key]->specialPrice = $priceHandler->getSpecialPrice();
        $similarCars[$key]->calculatedPrice = $priceHandler->calculatePrice();

        /**
         * new price table
         */
        $similarCars[$key]->priceTable = $priceHandler->getPriceTable();
        $similarCars[$key]->deliveryPrice = $priceHandler->getDeliveryPrice();

        $dealClass = Data::getMainTerm($deal->ID, 'class');

        $dealCities = get_the_terms($deal->ID, 'cities');
        $orderCitiesArr = [];
        $orderCities = '';
        foreach ($dealCities as $city ){
            $orderCitiesArr[] = $city->name;
        }

        //Adding  label  to car
        $labels = get_the_terms($deal->ID, 'label');
        $labelsArr = [];

        foreach ($labels as  $label){
            $label->color_text =  get_field('color_text', $label);
            $label->color_label =  get_field('color_label', $label);
            $labelsArr[] = get_object_vars($label);
        }
        $similarCars[$key]->car_labels = $labelsArr;

        $orderCities = implode(',', $orderCitiesArr);
        $similarCars[$key]->orderCities = $orderCities;
        $similarCars[$key]->class_label = $dealClass->name;

    }
}
$context['similarCarsLabel'] = __($context['car']->acf['similar_cars']['label']);
$context['similarCars'] = $similarCars;

//Adding  label  to car
$car =  $context['car'];

$labels = get_the_terms($car->ID, 'label');
$labelsArr = [];
foreach ($labels as  $label){
    $label->color_text =  get_field('color_text', $label);
    $label->color_label =  get_field('color_label', $label);
    $labelsArr[] = get_object_vars($label);
}
$car->car_labels = $labelsArr;


#h1 - 20.03.2018
$h1 = $context['car']->acf['title_h1']['value'];
if ( ! empty($h1) ) {
        $context['car']->title_h1 = $h1;
}

//  <span class="days">{{ lang.for_day }}</span>
if (isset($context['lang']['for_day_png'])) {
    $context['lang']['day_src'] = THEME_URI . '/public/images/' . $context['lang']['for_day_png'] . '.png';
}

add_head_footer($context);

$core->render('single-car.twig', $context);