<?php
/**
 * @global Akni\core\CoreTheme
 */


global $core;

use Akni\app\core\CoreTheme;
use Akni\app\Model\FilterCat;
use Akni\app\Model\MainAjax;

/**
 * Use composer
 */
if (file_exists($composer_autoload = __DIR__ . '/vendor/autoload.php')) {
    require_once $composer_autoload;
} else {
    _e('Install composer for current work');
    exit;
}

define('THEME_URI', get_template_directory_uri());

//ini_set('error_reporting', E_ALL);
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);

$core = new CoreTheme();
$mainAjax = new MainAjax($core);
FilterCat::init();

add_action('init', array('Akni\app\Helper\Ametabox', 'init'));

/**
 * Special block for translating default plugins.
 * @param $text
 * @return WP_Post
 */
function customTranslate($text)
{
    $str = $text;
    $text = qtranxf_split($text);
    if (qtrans_getLanguage() == "ua") {
        $str = $text['ua'];
    } elseif (qtrans_getLanguage() == "ru") {
        $str = $text['ru'];
    } elseif (qtrans_getLanguage() == "en") {
        $str = $text['en'];
    }
    return $str;
}

/**
 * $context[ 'head' => ..., 'footer' => ... ];
 */
function add_head_footer(& $context = [])
{
    ob_start();
    echo '<!-- local head //-->';
    wp_head();
    echo '<!-- end head //-->';
    $context['head'] = apply_filters('header_content', ob_get_clean());

    ob_start();
    echo '<!-- local footer //-->';
    wp_footer();
    echo '<!-- end footer //-->';
    $context['footer'] = ob_get_clean();
}


/**
 * SEO SOLUTIONS
 */
function addSeoScript()
{
    wp_enqueue_script('seo', '/wp-content/themes/Akni/public/js/switch.js');
}

add_action('admin_enqueue_scripts', 'addSeoScript');

remove_action('wp_head', 'wp_shortlink_wp_head');
remove_action('wp_head', 'wp_generator');

add_filter('login_errors', create_function('$a', "return 'Ошибка авторизации';"));


// flush_rewrite_rules();

add_action('init', 'filter_endpoint');
function filter_endpoint()
{
    $mask = EP_PAGES | EP_SEARCH; //EP_ROOT |
    // EP_SEARCH - поиска
    // EP_CATEGORIES - рубрик
    // EP_AL

    add_rewrite_endpoint('filter', $mask);
}


// Remove - Canonical
function remove_canonical()
{
    preg_match_all('/.*(filter).*/m', $_SERVER['REQUEST_URI'], $matches, PREG_SET_ORDER, 0);
    if (count($matches) > 0) {
        add_filter('wpseo_canonical', '__return_false');
    }
}
add_action('wp', 'remove_canonical');

$sort_1= preg_match_all('/.*(sort-by-high).*/m', $_SERVER['REQUEST_URI'], $matches, PREG_SET_ORDER, 0);
if ($sort_1 == true){
    $str = $_SERVER['REQUEST_URI'];
    $str1 = '';

    $test = preg_replace('/sort-by-high/', $str1 ,$str);
    wp_redirect(get_site_url().$test);
}

$sort_2= preg_match_all('/.*(sort-by-low).*/m', $_SERVER['REQUEST_URI'], $matches, PREG_SET_ORDER, 0);
if ($sort_2 == true){
    $str = $_SERVER['REQUEST_URI'];
    $str1 = '';

    $test = preg_replace('/sort-by-low/', $str1 ,$str);
    wp_redirect(get_site_url().$test);
}

$sort_3= preg_match_all('/.*(sort-by-popularity).*/m', $_SERVER['REQUEST_URI'], $matches, PREG_SET_ORDER, 0);
if ($sort_3 == true){
    $str = $_SERVER['REQUEST_URI'];
    $str1 = '';

    $test = preg_replace('/sort-by-popularity/', $str1 ,$str);
    wp_redirect(get_site_url().$test);
}

function canonical()
{
    remove_action('wp_head', 'rel_canonical');
}

add_action('init', 'canonical', 99);


// Remove - noindex


preg_match_all('/.*(filter).*/m', $_SERVER['REQUEST_URI'], $matches, PREG_SET_ORDER, 0);
if (count($matches) > 0) {
    remove_action('wp_head', 'noindex');
}


function yoast_no_home_noindex($string = "")
{
    if (is_home() || is_front_page()) {
        $string = "index,follow";
    }
    return $string;
}

remove_action('wp_head', 'noindex', -1);

add_filter('wpseo_robots', 'yoast_seo_robots_remove_search');
function yoast_seo_robots_remove_search($robots)
{
    return $robots;
}


add_filter('user_trailingslashit', 'no_page_slash', 70, 2);
function no_page_slash($string, $type)
{
    global $wp_rewrite;
    preg_match_all('/.*(avtopark).*/m', $_SERVER['REQUEST_URI'], $matches, PREG_SET_ORDER, 0);
    if (count($matches) > 0) {
    } else {
        if ($type == 'page' && $wp_rewrite->using_permalinks() && $wp_rewrite->use_trailing_slashes)
            $string = untrailingslashit($string);
    }
    preg_match_all('/.*(filter).*/m', $_SERVER['REQUEST_URI'], $matches, PREG_SET_ORDER, 0);
    if (count($matches) > 0) {
        if ($type == 'page' && $wp_rewrite->using_permalinks() && $wp_rewrite->use_trailing_slashes)
            $string = untrailingslashit($string);
    }
    return $string;
}

if( function_exists('acf_add_options_page') ) {

    acf_add_options_page(array(
        'page_title' 	=> 'Theme General Settings',
        'menu_title'	=> 'Theme Settings',
        'menu_slug' 	=> 'theme-general-settings',
        'capability'	=> 'edit_posts',
        'redirect'		=> false
    ));

    acf_add_options_sub_page(array(
        'page_title' 	=> 'Theme Footer Settings',
        'menu_title'	=> 'Footer',
        'parent_slug'	=> 'theme-general-settings',
    ));
}


