<?php
/**
 * Created by PhpStorm.
 * User: icefier
 * Date: 07.10.16
 * Time: 0:49
 */
/* Template Name: Single-article Template */

global $core;
global $post;

$context = $core->get_context();
$model  = $core->getModel();
$config = $core->getConfig();

if ($post) {
    $model->setResult(['0'=>$post]);
    $model->formattedACF();
    $model->setMainThumbnailUrls();

    $news = $model->getResult();
    if (!empty($news)) {
        foreach ($news as &$article) {
            if($article->post_date){
                $article->post_date = $core->specialDateFormat($post->post_date);
            }
        }
    }
    $context['article'] = $news[0];
    #meta data
    $context['meta_description'] = get_post_meta($context['article']->ID,'_yoast_wpseo_metadesc', true);
    $context['meta_title'] = get_post_meta($context['article']->ID,'_yoast_wpseo_title', true);
    if ($context['meta_title'] =='') {
        $context['meta_title'] = $context['article']->post_title;
    }
}

add_head_footer($context);
$core->render('single.twig', $context);