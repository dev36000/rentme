<?php

/* Template Name: Search Template */

global $core;
global $post;

$context = $core->get_context();
$model  = $core->getModel();
$config = $core->getConfig();

$core->render('searchform-template.twig', $context);
