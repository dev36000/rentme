<?php

/* Template Name: Catalog Template */
use Akni\app\Helper\Data;
use Akni\app\Model\CarPrice;
use Akni\app\Model\MainAjax;
global $core;
global $post;


$context = $core->get_context();
$model  = $core->getModel();
$config = $core->getConfig();
$showConfig = $config->getConfig('show', 'show');
$filterTypes = $config->getConfig('filter_types')['filter_types'];
$priceHandler = new CarPrice();
$prices = [];

if ($post && $post->post_type == 'page') {
    $model->setResult(['0'=>$post]);
    $model->formattedACF();
    $post = $model->getResult();
    $context['page'] = $post[0];

    #meta data
    $context['meta_description'] = get_post_meta($context['page']->ID,'_yoast_wpseo_metadesc', true);
    $context['meta_title'] = get_post_meta($context['page']->ID,'_yoast_wpseo_title', true);
    if ($context['meta_title'] =='') {
        $context['meta_title'] = $context['page']->post_title;
    }

}

#cars block

$cars = [];
$mainTerm = Data::getMainTerm($context['page']->ID, 'cities');
$mainTermTitle = get_field('where_text',"cities_{$mainTerm->term_id}");
$pageTitle = $mainTermTitle ? $mainTermTitle : $context['city_info']['name'];
$context['pageTitle'] = $pageTitle;

$context['breadcrumbs']['0'] = [
    'url' => $context['city_urls']['catalog'],
    'title' => $context['lang']['cars_text'] . ' ' . $pageTitle
];

if ($showConfig['cars']) {
    $model->setArgs($showConfig['cars']);
    $model->setPosts();
    $model->setMainThumbnailUrls([300, 300]);
    $model->setPostUrls();
    $model->formattedACF();
    $cars = $model->getResult();

    if (!empty($cars)) {
        foreach($cars as $key=>$car) {
            $priceHandler->setProduct($car);

            $cars[$key]->regularPrice = $priceHandler->getRegularPrice();
            $cars[$key]->specialPrice = $priceHandler->getSpecialPrice();
            $cars[$key]->calculatedPrice = $priceHandler->calculatePrice();
            $cars[$key]->priceTable = $priceHandler->getPriceTable();
            $cars[$key]->deliveryPrice = $priceHandler->getDeliveryPrice();

            $carClass = Data::getMainTerm($car->ID, 'class');
            if (!$carClass->name) {
                unset($cars[$key]);
                continue;
            }

            $dealCities = get_the_terms($car->ID, 'cities');
            $orderCitiesArr = [];
            $orderCities = '';
            foreach ($dealCities as $city ){
                $orderCitiesArr[] = $city->name;
            }

            $orderCities = implode(',', $orderCitiesArr);

            $cars[$key]->orderCities = $orderCities;
            $cars[$key]->class_label = $carClass->name;

            $prices[] = $cars[$key]->calculatedPrice;
        }
    }

    if (!empty($cars)) {
        $context['cars']['title'] = $mainTermTitle;
        $context['cars']['cars'] =  array_chunk($cars , 9);
    }
}

// filters...
$_POST['data'] = array( 
    'params' => array( 
        'class' => array(
            'class', '12'
        )));

$cars = $mainAjax->carFiltering();
unset($_POST['data']);


$context['cars']['cars'] = array_chunk($cars , 9);



#Filters block
$context['car_classes'] = get_terms('class', ['hide_empty' => true,]);


$fieldsGroups = acf_get_field_groups();
$fields = [];
$filters = [];

if (!empty($fieldsGroups)) {
    foreach ($fieldsGroups as $key=>$group) {
        if (strtolower($group['description']) =='main_filters' || strtolower($group['description']) =='other_filters') {
            $fields[$group['description']] = acf_get_fields($group['key']);
        }
    }
}

if (!empty($fields)) {
    foreach ($fields as $group) {
        foreach ($group as $field) {
            if (in_array($field['type'],$filterTypes)) {
                $filters[$field['name']]['label'] = __($field['label']);
                $filters[$field['name']]['name'] = $field['name'];
                $filters[$field['name']]['type'] = $field['type'];
                $filters[$field['name']]['values'] =  Data::getCustomPostAllMetaFieldValues($field['name'],'car','publish');
                asort($filters[$field['name']]['values']);
            }
        }
    }
}
$context['price_min'] = min($prices);
$context['price_max'] = max($prices);
if (!empty($filters)) {
    $context['filters'] = $filters;

}

#h1 - 20.03.2018
// <h1 class="page_title">{{ lang.cars_text }} {{ pageTitle }}</h1>
/*$h1 = $context['page']->acf['title_h1']['value'];
if ( ! empty($h1) ) {
        $context['lang']['title_h1'] = $h1;
}*/

//  <span class="days">{{ lang.for_day }}</span>
if (isset($context['lang']['for_day_png'])) {
    $context['lang']['day_src'] = THEME_URI . '/public/images/' . $context['lang']['for_day_png'] . '.png';
}

add_head_footer($context);
$core->render('catalog-template.twig', $context);
