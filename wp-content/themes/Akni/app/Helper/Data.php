<?php

namespace Akni\app\Helper;

class Data
{

    /**
     * usort orderSort method for sortFields function.
     * @param $a
     * @param $b
     * @return bool
     */
    public static function orderSort($a, $b)
    {
        return $a['order'] > $b['order'];
    }

    /**
     * @param $string
     * @param string $allow
     * @return string
     * Function for clear text from text
     */
    public static function cleanString($string, $allow = '')
    {
        return trim(strip_tags($string, $allow));
    }

    /**
     * @param $attachment_id
     * @return mixed
     * Return attachment images
     */
    public static function getAttachmentMeta($attachment_id)
    {
        $alt = get_post_meta(
            $attachment_id, '_wp_attachment_image_alt', true
        );
        if ($alt == '') {
            $alt = explode(
                '.', basename(get_attached_file($attachment_id)))[0];
        }
        return $alt;
    }

    /**
     * This method need to remove html tags and rounded whitespaces.
     * @param $value
     * @return string
     */
    public static function clearString($value)
    {
        return trim(strip_tags($value));
    }


    /**
     * Sort fields by order value.
     * @param $arr
     * @return mixed
     */
    public static function sortFields($arr)
    {
        uasort($arr, function ($a, $b) {
            return $a['order'] > $b['order'];
        });
        return $arr;
    }

    /**
     * Main term getter.
     * @param $postId
     * @param $termName
     * @return bool|\WPSEO_Primary_Term
     */
    public static function getMainTerm($postId, $termName)
    {
        if ($postId !=0 && $termName !='') {
            $terms = wp_get_post_terms($postId, $termName);
            if (count($terms) == 1) {
                return $terms[0];
            } elseif (count($terms) > 1)  {
                if ( class_exists('WPSEO_Primary_Term') ) {
                    $primaryTerm = new \WPSEO_Primary_Term( $termName, $postId);
                    $mainTerm = get_term_by( 'id', $primaryTerm->get_primary_term(), $termName);
                    if ($mainTerm) {
                        return $mainTerm;
                    }
                }

            }
        }
        return false;
    }

    /**
     * Get all term getter.
     * @param $postId
     * @param $termName
     * @return array|bool|false|\WP_Term
     */
    public static function getAllTerm($postId, $termName)
    {
        if ($postId !=0 && $termName !='') {
            $terms = wp_get_post_terms($postId, $termName);
            if (count($terms) == 1) {
                return $terms[0];
            } elseif (count($terms) > 1)  {
                return $terms;
            }
        }
        return false;
    }

    /**
     *  Method vailidate product special.
     * @param $product
     * @return bool
     */
    public static function isSpecial($product)
    {
        $today = time();

        if(!$product->acf['speical_price']['value']) {
            return false;
        } else {
            if ($product->acf['from']['value']) {
                if ($today < strtotime($product->acf['from']['value'])) {
                    return false;
                }
            }
            if ($product->acf['to']['value']) {
                if ($today > strtotime($product->acf['to']['value'])) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Count post with type @var $postType, in @var $terms.
     * @param $postType
     * @param array $terms
     * @return int
     */
    public static function getPostInTermCounter($postType,  $terms = [])
    {
        if (post_type_exists($postType) && !empty($terms)) {
            $args = [
                'post_type' => $postType,
                'post_status' => 'publish',
                'posts_per_page' => -1
            ];
            $taxQuery['relation'] =  'AND';

            foreach($terms as $tax => $term){
                $taxQuery[] = array(
                    'taxonomy' => $tax,
                    'field' => 'id',
                    'terms' => [$term]
                );

            }
            $args['tax_query'] =  $taxQuery;

            $posts = get_posts($args);

            if ($postType = 'cars') {
                foreach ($posts as $key=>$car) {
                    $carClass = Data::getMainTerm($car->ID, 'class');
                    if (!$carClass->name) {
                        unset($posts[$key]);
                        continue;
                    }
                }
            }

            $num = count($posts);
            return (int)$num;
        }

        return 0;
    }

    /**
     * @param $needle string
     * @param $haystack string
     * @return bool
     */
    public static function isStartFrom($needle, $haystack)
    {
        return (substr($haystack, 0, strlen($needle))==$needle);
    }

    public static  function getCustomPostAllMetaFieldValues( $key = '', $type = 'post', $status = 'publish' ) {
        global $wpdb;
        if( empty( $key ) ) {
            return false;
        }
        $res = $wpdb->get_col( $wpdb->prepare( "
            SELECT DISTINCT pm.meta_value FROM {$wpdb->postmeta} pm
            LEFT JOIN {$wpdb->posts} p ON p.ID = pm.post_id
            WHERE pm.meta_key = '%s'
            AND p.post_status = '%s'
            AND p.post_type = '%s'
        ", $key, $status, $type ) );
        return array_diff($res, ['']);
    }
}