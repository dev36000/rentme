<?php 
namespace Akni\app\Helper;

defined( 'ABSPATH' ) or die();

class Ametabox
{
    public static function init()
    {
       // add_action('add_meta_boxes', array('Ametabox', 'add_meta' ), 1);
        add_action('add_meta_boxes_page', array('Akni\app\Helper\Ametabox', 'add_meta' ));
        add_action('save_post',  array('Akni\app\Helper\Ametabox', 'update_meta' ), 0);
    }

    public static function add_meta() 
    {
	    add_meta_box( 'Ametabox', 'Редирект', array('Akni\app\Helper\Ametabox', 'html'), array('page'), 'side', 'default', 'high' );
    }

    public static function html($post)
    {
        $checked = esc_html( get_post_meta( $post->ID, 'urifilter', 1) );
        $value   = $checked === 'yes' ? 'checked="checked"' : '';
        ?>
        <div>
            <label>Редирект фильтра</label>
            <input type="checkbox" id="redirect_filter" name="redirect_filter" value="yes" <?php echo $value; ?> placeholder="Добавить в слайдер" />
        </div>

        <input type="hidden" name="Ametabox_nonce" value="<?php echo wp_create_nonce(__FILE__); ?>" />
        <?php
    }

    

    public static function update_meta($post_id)
    {
        if ( ! isset($_POST['Ametabox_nonce']) || ! wp_verify_nonce($_POST['Ametabox_nonce'], __FILE__) ) return false; 
        if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE  ) return false; 
        if ( ! current_user_can('edit_post', $post_id) ) return false; 

        $value = isset($_POST['redirect_filter']) ? filter_var( $_POST['redirect_filter'] ) : false; 
        
        if( empty($value) ) {
			delete_post_meta( $post_id, 'urifilter' ); 
        }
		update_post_meta( $post_id, 'urifilter', strip_tags($value) );
	    return $post_id;
    }

}



