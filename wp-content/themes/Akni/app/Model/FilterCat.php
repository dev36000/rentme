<?php
/**
 * /wp-content/themes/Akni/page-templates/catalog-page.php
 * /wp-content/themes/Akni/views/modules/catalog/_filters.twig
 */

namespace Akni\app\Model;

use Akni\app\Helper\Data;

class FilterCat
{

    private static $meta_description = '';
    private static $filters = [];
    private static $query_string = [];
    private static $metadata = [];
    private static $isreplace = false;
    private static $salt = '!bp1yW-aq8P@j';


    public static function init()
    {
        add_action("save_post_filter-car", ['Akni\app\Model\FilterCat', 'save_hash2'], 10, 2);
    }


    /**
     * Получаем автомобили
     *
     * @return (array) $cars
     */
    public static function carFiltering()
    {
        global $core;

        $specialFilters = [];

        $config = $core->getConfig();
        $carsConfig = $config->getConfig('show', 'show')['cars'];

        $taxQuery['relation'] = 'AND';

        if ((int)$_SESSION['current_city']) {
            $taxQuery[] = [
                'taxonomy' => 'cities',
                'field' => 'id',
                'terms' => (int)$_SESSION['current_city'],
            ];
            $carsConfig['tax_query'] = $taxQuery;

        }


       /* $filter =  $_GET(['price']);
              var_dump($filter);*/
        if (!empty(self::$filters)) {

            if (isset(self::$filters['class']) && is_array(self::$filters['class'])) {
                $carsConfig = self::_setCarClass($carsConfig, self::$filters['class']);
               /* var_dump($carsConfig);*/
                unset (self::$filters['class']);
            }


            if (!empty(self::$filters)) {
                $carsConfig = self::_setFilters($carsConfig);

            }

        }
        $filters = $_SESSION;
        $filterPrice = explode(',', $filters['price'][1]);
        $specialFilters['price'] = $filterPrice;

        $cars = self::_getFilteredCars($carsConfig, $specialFilters);

        return $cars;

    }


    /**
     * делаем подмену данных при наличии фильтра
     */
    public static function replaceData($post_id, & $context)
    {
        if (!$post_id) return false;

        $post = get_post($post_id);
        $post_data = sanitize_post($post, 'display');
        $locale = str_replace(['_RU', '_US'], '', get_locale()); // fix:...
        $acf = get_field_objects($post_id);

        if (isset($acf['title_H1']) && $acf['title_H1']['value']) {
            if (strstr($acf['title_H1']['value'], '(RU)')) {

            } else {
                $context['lang']['title_h1'] = sanitize_text_field($acf['title_H1']['value']);
            }
        }

        if (isset($acf['annotations']) && $acf['annotations']['value']) {
            $context['page']->acf['annotations']['value'] = $acf['annotations']['value'];
        }

        if ($post_data->post_title) {
            if (strstr(__($post_data->post_title), '(RU)')) {

            } else {
                $context['page']->post_title = __($post_data->post_title);
                /*            $context['meta_title']         = sanitize_text_field( __( $post_data->post_title ) );*/ // Старый вариант фомирования тайтла страниц фильров
            }

            if (strstr(get_post_meta($post_id, '_yoast_wpseo_title', true), '(RU)')) {
                $context['meta_title'] = $context['lang']['title_h1'];
            } else {
                $context['meta_title'] = sanitize_text_field(get_post_meta($post_id, '_yoast_wpseo_title', true)); // Возвращаем в тайтл сео данные
            }
        }

        if ($post_data->post_content) {
            if (strstr(__($post_data->post_content), '(RU)')) {

            } else {
                $context['page']->post_content = __($post_data->post_content);
            }

        }

    }


    /**
     * получаем данные SEO фильтра
     * до рендерига header
     */
    public static function getMetaSEO($post_id, & $context)
    {
        self::close_index($post_id);

        if (!$post_id) return false;

        $post = get_post($post_id);
        $locale = str_replace(['_RU', '_US'], '', get_locale());
        $acf = get_field_objects($post_id);

        self::$isreplace = true;

        $twitter_title = sanitize_text_field(get_post_meta($post_id, '_yoast_wpseo_twitter-title', true));
        $twitter_description = sanitize_text_field(get_post_meta($post_id, '_yoast_wpseo_twitter-description', true));
        self::$metadata['twitter_image'] = get_post_meta($post_id, '_yoast_wpseo_twitter-image', true);
        // yoast_wpseo_focuskw_text_input
        self::$metadata['meta_title'] = sanitize_text_field(get_post_meta($post_id, '_yoast_wpseo_title', true));
        self::$metadata['meta_description'] = sanitize_text_field(get_post_meta($post_id, '_yoast_wpseo_metadesc', true));

        self::$metadata['twitter_title'] = $twitter_title ? $twitter_title : self::$metadata['meta_title'];
        self::$metadata['twitter_description'] = $twitter_description ? $twitter_description : self::$metadata['meta_description'];

        wp_reset_postdata();

        return self::$metadata;
    }


    /**
     *  1. https://rentme.com.ua/golovna-kiyiv/catalog/ -
     * на такой странице, где нет примененных фильтров или без метатега
     * или чтобы был index, follow
     *
     *  2. https://rentme.com.ua/golovna-kiyiv/catalog/filter/class-suv-premium -
     * на странице, которая сгенерирована под определенный набор фильтров
     * и для неё уже указаны  текст и данные тоже либо нужен метатег
     * index, follow либо можно без метатега вообще
     *
     *  3. https://rentme.com.ua/golovna-kiyiv/catalog/filter/class-suv-premium/passengers_num-5 - а вот пример страницы, которая генерится фильтром, но для неё не прописаны данные (текст, заголовки, описания).
     * Вот именно такие должны по умолчанию быть закрыты.
     *
     * запрещаем индексирование
     *  + есть данные запроса
     *  + пост с мета данными не создан \не найден)
     */
    public static function close_index($post_id)
    {
        if (!empty(self::$query_string) && !$post_id) {
            /* add_filter( 'wpseo_robots', function() { return 'noindex,nofollow'; });*/
        }
    }


    /**
     * заменяем текущие SEO данные страницы на данные фильтра (если есть)
     * TD: $keywords = apply_filters( 'wpseo_metakeywords', trim( $keywords ) );
     * - /wp-content/plugins/wordpress-seo/frontend/class-frontend.php
     */
    public static function replaceSEO(& $context)
    {
        if (!self::$isreplace) return false;

        if (isset(self::$metadata['meta_description']) && self::$metadata['meta_description']) {
            $context['meta_description'] = self::$metadata['meta_description'];
        }
        if (isset(self::$metadata['meta_title']) && self::$metadata['meta_title']) {
            $context['meta_title'] = self::$metadata['meta_title'];
        }
    }


    /**
     * замена значений после формирования HEAD
     */
    public static function replaceOG($post_id)
    {
    }

    /**
     * перед формированием HEAD
     */
    public static function replace_metadesc()
    {
    }


    // 25.07.2018
    public static function is_ajax()
    {
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&
            strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
        ) {
            return true;
        }
        return false;
    }

    /**
     * Method to set filters
     *
     * @param $carsConfig
     * @param $filters
     * @return array
     */
    private static function _setFilters($carsConfig)
    {
        $metaQuery['relation'] = 'AND';
        foreach (self::$filters as $key => $val) {
            if (!empty($val)) {
                if (count($val) > 1) {
                    $metaQuery[] = [
                        'key' => $key,
                        'value' => $val,
                        'compare' => 'IN',
                    ];
                } else {
                    $metaQuery[] = [
                        'key' => $key,
                        'value' => $val[0],
                        'compare' => '=',
                    ];
                }
            }
        }

        $carsConfig['meta_query'] = $metaQuery;
        return $carsConfig;
    }

    /**
     * Method to set car class
     *
     * @param $carsConfig
     * @param $classes
     * @return mixed
     */
    private static function _setCarClass($carsConfig, $classes)
    {

        $taxQuery[] = [
            'taxonomy' => 'class',
            'field' => 'slug',
            'terms' => $classes //explode(',', $classes)
        ];
        $carsConfig['tax_query'][] = $taxQuery;

        return $carsConfig;
    }

    /**
     * Method to get filtered cars
     *
     * @param $carsConfig
     * @param array $specialFilters
     * @return array
     */
    private static function _getFilteredCars($carsConfig, array $specialFilters = [])
    {
        global $core;

        $model = $core->getModel();
        $priceHandler = new CarPrice();
        $model->setArgs($carsConfig);
        $model->setPosts();
        $model->setMainThumbnailUrls([300, 300]);
        $model->setPostUrls();
        $model->formattedACF();
        $cars = $model->getResult();

        if (!empty($specialFilters)) {
            if (!empty($specialFilters['price'])) {
                $min_price = min($specialFilters['price']);
                $max_price = max($specialFilters['price']);
            }
        }

        if (!empty($cars)) {
            foreach ($cars as $key => $car) {
                $priceHandler->setProduct($car);

                $cars[$key]->regularPrice = $priceHandler->getRegularPrice();
                $cars[$key]->specialPrice = $priceHandler->getSpecialPrice();
                $cars[$key]->calculatedPrice = $priceHandler->calculatePrice();

                /**
                 * new price table
                 */
                $cars[$key]->priceTable = $priceHandler->getPriceTable();
                $cars[$key]->deliveryPrice = $priceHandler->getDeliveryPrice();

                $carClass = Data::getMainTerm($car->ID, 'class');
                if (!$carClass->name) {
                    unset($cars[$key]);
                    continue;
                }
                $dealCities = get_the_terms($car->ID, 'cities');
                $orderCitiesArr = [];

                foreach ($dealCities as $city) {
                    $orderCitiesArr[] = $city->name;
                }

                $orderCities = implode(',', $orderCitiesArr);
                $cars[$key]->orderCities = $orderCities;
                $cars[$key]->class_label = $carClass->name;

                //Adding  label  to car
                $labels = get_the_terms($car->ID, 'label');
                $labelsArr = [];

                foreach ($labels as  $label){
                    $label->color_text =  get_field('color_text', $label);
                    $label->color_label =  get_field('color_label', $label);
                    $labelsArr[] = get_object_vars($label);
                }
                $cars[$key]->car_labels = $labelsArr;

                if (isset($min_price) && $min_price != 0 && isset($max_price) && $max_price != 0) {
                    if ($cars[$key]->calculatedPrice < $min_price || $cars[$key]->calculatedPrice > $max_price) {
                        unset ($cars[$key]);
                    }
                }
            }
        }
        return $cars;
    }




    // ==============================================================================
    // ======================================================================= 2 ====
    // ==============================================================================

    /*
    browser -> server
    get query
    get hash    - для имени файла кеша
    parse param - для WP запроса
    */


    /**
     * проверяем корректность параметров GET
     * инициируем self::$filters
     * $q_params - сгруппированные данные acf_get_field_groups | acf_get_fields
     */
    public static function init_params2($q_params)
    {
        $filters = [];
        $query_string = [];
        $get_filter = self::parse_params();
        $car_classes = get_terms('class', ['hide_empty' => true]);

        foreach ($car_classes as $item) {
            $q_params['class'][] = $item->slug;
        }

        foreach ($q_params as $key => $choices) {
            if (isset($get_filter[$key]) && $get_filter[$key] != '') {
                // есть ли значение параметра
                $params = $get_filter[$key];
                sort($choices);
                foreach ($choices as $choice) {
                    if (in_array($choice, $params)) {
                        $filters[$key][] = substr(strip_tags($choice), 0, 30);
                    }
                }
                if (!empty($filters[$key])) {
                    $query_string[$key] = implode(',', $filters[$key]);
                }
            }
        }

        if (!empty($query_string)) {
            ksort($query_string);

            self::$query_string = $query_string;
        }
        // echo '$q_params';
        // _log($q_params);
        // echo '$query_string';
        // _log(self::$query_string);

        self::$filters = !empty($filters) ? $filters : false;

        return true;
    }


    /**
     *  Форматируем данные запроса AJAX
     */
    public static function init_ajax_params()
    {
        $get_filter = self::parse_params();

        foreach ($get_filter as $key => $choices) {
            if ($choices && !empty($choices)) {
                sort($choices);
                foreach ($choices as $item) {
                    $filters[$key][] = substr(strip_tags($item), 0, 30);
                }
            }
            if (!empty($filters[$key])) {
                $query_string[$key] = implode(',', $filters[$key]);
            }
        }
        if (!empty($query_string)) {
            ksort($query_string);
            self::$query_string = $query_string;
        }

        self::$filters = !empty($filters) ? $filters : false;

        return true;
    }

    /**
     * получаем ID поста фильтра(filter-car), на основани параметров GET
     *  // _log( self::$filters );
     */
    public static function get_search_post_id2()
    {
        // attribute_condition !!!!!
        global $wpdb;

        $cities = false;
        if (isset($_SESSION['current_city']) && $_SESSION['current_city']) {
            $cities = (int)$_SESSION['current_city'];
        }

        $uri_str = self::filters_to_string(self::$filters, $cities);

        $hash = MD5($uri_str . self::$salt);

        $query = $wpdb->prepare("SELECT id_post FROM akni_filter_index  WHERE search = %s", $hash);

        $result = $wpdb->get_var($query);

        return $result;
    }

    protected static function filters_to_string($filters, $cities = false)
    {
        $uri_str = '';
        if (!empty($filters)) {
            ksort($filters);
        } else {
            $filters = [];
        }

        if ($cities) {
            $filters['cities'] = (int)$cities;
        }

        foreach ($filters as $key => $var) {
            $uri_str .= '/' . $key . '-' . (is_array($var) ? implode('-', $var) : $var);
        }

        return $uri_str;
    }


    /**
     * парсим параметры запроса URI
     *
     */
    public static function parse_params()
    {
        $array_var = self::get_query();
        $params = [];

        $array_var = explode('/', $array_var);

        if (is_array($array_var)) {
            foreach ($array_var as $item) {
                $_param = explode('-', $item);
                $key = array_shift($_param);
                sort($_param);
                $params[$key] = $_param;
            }
            ksort($params);
        }
        return $params;
    }


    /**
     *  возвращаем имя файла кеша
     */
    public static function file_name_cache($isquery = false)
    {
        return TEMPLATEPATH . '/cache/' . self::hash($isquery) . '.cache';
    }

    /**
     *  массив параметров в строку
     */
    public static function hash($isquery = true)
    {
        $uri_str = '';
        if ($isquery) {
            $_str = self::get_query();
            if (isset($_SESSION['current_city']) && $_SESSION['current_city']) {
                $uri_str .= '/cities-' . (int)$_SESSION['current_city'];
            }
            $uri_str .= '/locale-' . get_locale();
        } else if (is_array(self::$filters) && !empty(self::$filters)) {
            ksort(self::$filters);
            // + cities + locale
            $filter = self::$filters;
            self::add_filetr_params($filter);
            $uri_str = self::filters_to_string($filter);
            // unset( $filter );
        }
        return MD5($uri_str . self::$salt);
    }


    /**
     * add filters: +citi +locale
     */
    public static function add_filetr_params(& $filter)
    {
        if (isset($_SESSION['current_city']) && $_SESSION['current_city']) {
            $params['cities'] = (int)$_SESSION['current_city'];
        }

        $params['locale'] = get_locale();
    }

    /**
     *  получаем параметры запроса
     *  filter/class-bussines-suv/transmission-A/
     */
    public static function get_query()
    {
        $params = '';
        if (!self::is_ajax()) {
            $params = substr(get_query_var('filter'), 0, 300);
        } else if (isset($_POST['data']) && isset($_POST['data']['uri']) && $_POST['data']['uri']) {
            $params = substr($_POST['data']['uri'], 0, 300);
        }
        $var = filter_var(rawurldecode($params), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH | FILTER_FLAG_STRIP_LOW);

        //$var = htmlspecialchars($var, ENT_QUOTES, 'UTF-8', false);
        return $var;
    }

    /**
     * Получаем данные для контекста страницы
     * title, h1, content
     * :AJAX
     */
    public static function get_context_data()
    {
        $locale = str_replace(['_RU', '_US'], '', get_locale()); // fix:...
        $data = [];
        $post_id = FilterCat::get_search_post_id2();
        // по умолчанию используем данные раздела | current_page
        if (!$post_id && isset($_POST['data']) && isset($_POST['data']['pid'])) {
            $post_id = absint($_POST['data']['pid']);
        }

        if ($post_id) {
            $post = get_post($post_id, OBJECT, 'display');
            $acf = get_field_objects($post_id);
            $meta_title = sanitize_text_field(get_post_meta($post_id, '_yoast_wpseo_title', true));


            if (strstr($meta_title, '(RU)')) {

            } else {
                $data['title'] = $meta_title;
            }
            if (strstr(__($post->post_content), '(RU)')) {

            } else {
                $data['.seo_block'] = str_replace(['\t', '  '], ' ', __($post->post_content));
            }
            if (strstr($acf['title_H1']['value'], '(RU)')) {

            } else {
                $data['.page_title'] = isset($acf['title_H1']) ? sanitize_text_field($acf['title_H1']['value']) : '';
            }
            $data['.annotations'] = isset($acf['annotations']) ? sanitize_text_field($acf['annotations']['value']) : '';


        }

        return $data;
    }


    /**
     * :filter:
     * генерируем и сохраняем ключ для фильтра
     * на основании параметров метатегов и выбраных таксономий (город, класс автомобиля)
     */
    public static function save_hash2($post_ID, $post)
    {
        global $wpdb;
        $filters = [];
        $uri_str = '';

        $class = get_the_terms($post_ID, 'class');

        if ($class) {
            $slug = [];
            foreach ($class as $item) {
                $slug[] = $item->slug;
            }
            sort($slug);
            $filters['class'] = $slug;
        }

        self::get_auto_attr($post_ID, $filters);
        $cities = false;
        $_cities = get_the_terms($post_ID, 'cities');
        if ($_cities && isset($_cities[0])) {
            $cities = (int)$_cities[0]->term_id;
        }

        $uri_str = self::filters_to_string($filters, $cities);
        // /class-premium-standard-suv/cities-3
        $hash = MD5($uri_str . self::$salt);

        $query = $wpdb->prepare("SELECT search FROM akni_filter_index  WHERE id_post = %d", $post_ID);

        if ($old_hash = $wpdb->get_var($query)) {
            // echo json_encode($filters);
            $wpdb->update('akni_filter_index', ['search' => $hash], ['id_post' => $post_ID], ['%s'], ['%d']);
        } else {
            $wpdb->insert('akni_filter_index', ['id_post' => $post_ID, 'search' => $hash], ['%d', '%s']);
            // echo $post_ID . ' insert ' .$hash ;

            $wpdb->show_errors();

            // $wpdb->print_error();
        }

        //die(json_encode($filters));
    }


    /**
     * получаем сортированный массив атрибутов(метатеги) поста (filter-car)
     *   атрибуты авто (для фильтров)
     * $filters: array
     */
    public static function get_auto_attr($post_ID, & $filters)
    {
        $fieldsGroups = acf_get_field_groups();

        $fields = [];
        $q_params = [];

        if (!empty($fieldsGroups)) {
            foreach ($fieldsGroups as $key => $group) {
                if (strtolower($group['description']) == 'main_filters' || strtolower($group['description']) == 'other_filters') {
                    $fields[$group['description']] = acf_get_fields($group['key']);
                }
            }
        }

        if (!empty($fields)) {
            foreach ($fields as $group) {
                foreach ($group as $field) {
                    if (in_array($field['type'], ["checkbox", "number", "radio", "select", "true_false"])) {
                        if (isset($field['choices'])) {
                            $val = get_post_meta($post_ID, $field['name'], true);
                            if ($val && $val != '-') {
                                $filters[$field['name']] = $val;
                            }
                        }
                    }
                }
            }
        }

        ksort($filters);
    }

}
