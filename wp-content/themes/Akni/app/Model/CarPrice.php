<?php

namespace Akni\app\Model;
use Akni\app\Helper\Data;
/**
 * This class work with product prices.
 * Class CarPrice.
 * @package Akni\app\Model
 */
class CarPrice
{
    /**
     * This is current product variable.
     * @var $_product
     */
    private $_product;

    /**
     * @param $from
     * @param $to
     * @return integer
     */
    private function getPriceForTable($from, $to)
    {
        $days = ceil(($from + $to)/2);
        $daysPrice =  $this->calculatePrice($days);
        $dayPrice = ceil($daysPrice / $days);
        return $dayPrice;
    }

    public function __construct()
    {

    }

    /**
     * Product setter.
     * @param $product
     */
    public function setProduct($product)
    {
        $this->_product = $product;
    }

    public function getRegularPrice()
    {
        $class = Data::getMainTerm($this->_product->ID, 'class');
        $regularPrice = get_field('car_class_price', "class_{$class->term_id}");
        return $regularPrice;
    }

    public function getSpecialPrice()
    {
        if (Data::isSpecial($this->_product)) {
            $specialPrice = $this->_product->acf['speical_price']['value'];
            if ($specialPrice < $this->getRegularPrice()) {
                return $specialPrice;
            }
        }
        return false;
    }

    /**
     * return calculated product price.
     * @param int $days
     * @return float|int
     */
    public function calculatePrice( $days=1 )
    {
        $regularPrice = $this->getRegularPrice();
        $specialPrice = $this->getSpecialPrice();
        $price_intervals = get_field('price_intervals', $this->_product->ID);
        $price = $specialPrice ? $specialPrice : $regularPrice;
        $rate = '';
        $maxDaysArr = [];

        if ($this->_product->acf['use_factor']['value']) {
            $class = Data::getMainTerm($this->_product->ID, 'class');
            $price_factor = get_field('price_factor', "class_{$class->term_id}");
            if ($days == 1) {
                return $price;
            }
                if (!empty($price_factor)) {
                    foreach ($price_factor as $key => $factor) {
                        $maxDaysArr[$key] = $factor['days_to'];
                    if ($days <= $factor['days_to']) {
                            $rate = $factor['coefficient'];
                            break;
                        }
                    }
                    if ($rate =='') {
                        if ($maxDaysArr) {
                            $max = array_keys($maxDaysArr, max($maxDaysArr))[0];
                            $rate = $price_factor[$max]['coefficient'];
                        }
                    }
                if ($rate !='') {
                    return ceil (((int)$price * $rate) * (int)$days) ;
                }
            }
        } elseif (!empty($price_intervals)) {
            $price = 0;
            $lastPrice = 0;
            foreach ($price_intervals as $key => $factor) {
                if ($days <= $factor['days_to']) {
                    $price = $factor['price'];
                    break;
                }
                $lastPrice = $factor['price'];
            }
            if ($price == 0) {
                if ($lastPrice != 0) {
                    $price = $lastPrice;
                }
            }
            return ceil ((int)$price * (int)$days);
        }

        return 0;
    }

    /**
     * добавляем стоимость услуг
     */
    public function calculatePriceOpt( $price=0, $options )
    {
        if (isset($this->_product->acf['add_options_auto']) 
                && is_array($this->_product->acf['add_options_auto']['value']) 
                && !empty($options)) { 
            $adv_opt = $this->_product->acf['add_options_auto']['value'];
            foreach ( $adv_opt as $item ) {
         
                if (isset($options[$item['id']])) {
                    $price = (int)$price +(int)$item['price'];
                }
            }
            return $price;
        }
        else {
            return $price;
        }
    }



    public function getPriceTable()
    {
        $class = Data::getMainTerm($this->_product->ID, 'class');
        $priceTable = [];
        $price_factor = get_field('price_factor', "class_{$class->term_id}");
        $price_intervals = get_field('price_intervals', $this->_product->ID);
        $use_factor = get_field('use_factor', $this->_product->ID);
        if ($use_factor) {
            $priceTable = $this->getClassPriceTable($price_factor);
        } elseif(!empty($price_intervals)) {
            $priceTable = $this->getProductPriceTable($price_intervals);
        }
        else {
            foreach ($price_factor as $column) {
                $priceTable[] = [
                    'date_from' => $column['days_from'],
                    'date_to' => $column['days_to'],
                    'price' => 0
                ];
            }
        }

        return $priceTable;
    }


    /**
     * Method to create price table by price intervals
     *
     * @param $price_intervals
     * @return array
     */
    protected function getProductPriceTable($price_intervals) {
        $priceTable = [];
        if (!empty($price_intervals)) {
            foreach ($price_intervals as $column) {
                $priceTable[] = [
                    'date_from' => $column['days_from'],
                    'date_to' => $column['days_to'],
                    'price' => $column['price']
                ];
            }
        }
        return $priceTable;
    }

    /**
     * Method to get class price table
     *
     * @param $price_factor
     * @return array
     */
    protected function getClassPriceTable($price_factor)
    {
        $priceTable = [];
        if (!empty($price_factor)) {
            foreach ($price_factor as $column) {
                $priceTable[] = [
                    'date_from' => $column['days_from'],
                    'date_to' => $column['days_to'],
                    'price' => $this->getPriceForTable($column['days_from'],$column['days_to'])
                ];
            }

        }
        return $priceTable;
    }

    /**
     * Method to get delivery price.
     * @return int|bool
     */
    public function getDeliveryPrice()
    {
        $class = Data::getMainTerm($this->_product->ID, 'class');
        $deliveryPrice = get_field('home_delivery', "class_{$class->term_id}");
        return $deliveryPrice;
    }

}