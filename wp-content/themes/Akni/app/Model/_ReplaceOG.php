<?php

namespace Akni\app\Model;
use Akni\app\Helper\Data;

class ReplaceOG
{
    private static $metadata = [];

    /**
     *  ['annotations']      = (isset( $acf['annotations']['value'] ) && $acf['annotations']['value'] )? $acf['annotations']['value'] : '';
     *  ['meta_description'] = get_post_meta( $post_id,'_yoast_wpseo_metadesc', true );
     *  ['meta_title']       = get_post_meta( $post_id,'_yoast_wpseo_title', true );
     */
    public static function init( $metadata )
    {
        self::$metadata = $metadata;
        
        if (defined('WPSEO_VERSION')) {
            if ( ! empty($metadata) ) {
                self::setmeta();
            } 
            else {
                self::clearmeta();
            }
            add_filter( 'header_content', function($content) {
                return preg_replace( '/^<!--.*?[Y]oast.*?-->$/mi', '', $content );
            });
        }
    }

    public static function clearmeta()
    {
        add_filter( 'wpseo_title', '__return_false', 999 );
        add_filter( 'wpseo_metadesc',  '__return_false', 999 );
        add_filter( 'wpseo_canonical', '__return_false', 999 );  
       
        add_filter( 'wpseo_opengraph_type', '__return_false', 999 );  
        add_filter( 'wpseo_opengraph_title', '__return_false', 999 );  
        add_filter( 'wpseo_opengraph_desc', '__return_false', 999 );  
        add_filter( 'wpseo_opengraph_site_name', '__return_false', 999 );  
        add_filter( 'wpseo_opengraph_url', '__return_false', 999 );  

        add_filter( 'wpseo_og_og_image', '__return_false', 999 );  
        add_filter( 'wpseo_og_og_locale', '__return_false', 999 );  
        add_filter( 'wpseo_og_article_publisher ', '__return_false', 999 );  
              
        add_filter( 'wpseo_twitter_title', '__return_false', 999 );  
        add_filter( 'wpseo_twitter_description', '__return_false', 999 );  
        add_filter( 'wpseo_twitter_image', '__return_false', 999 );  
        add_filter( 'wpseo_twitter_card_type', '__return_false', 999 );
        //  remove_all_actions( 'wpseo_opengraph', 99 );
        //add_filter( 'wpseo_robots', function() { return 'noindex,nofollow'; });

        add_filter( 'header_content', function($content) {
            return preg_replace( '/<!--.*[-]->/', '', $content );
        });
        
    }

    protected static function setmeta()
    {
     
        add_filter( 'wpseo_title',     [ 'Akni\app\Model\ReplaceOG', 'replace_wpseo' ] );
        add_filter( 'wpseo_metadesc',  [ 'Akni\app\Model\ReplaceOG', 'replace_wpseo' ] );
        add_filter( 'wpseo_canonical', [ 'Akni\app\Model\ReplaceOG', 'replace_wpseo'] );
       // add_filter( 'wpseo_og_og_image', ['Akni\app\Model\ReplaceOG', 'replace_wpseo' ] );
        add_filter( 'wpseo_og_og_title', ['Akni\app\Model\ReplaceOG', 'replace_wpseo' ] );

        add_filter( 'wpseo_twitter_title', [ 'Akni\app\Model\ReplaceOG', 'replace_wpseo'] );
        add_filter( 'wpseo_twitter_description', [ 'Akni\app\Model\ReplaceOG', 'replace_wpseo'] );
        add_filter( 'wpseo_twitter_image', [ 'Akni\app\Model\ReplaceOG', 'replace_wpseo'] ); 

    }
    
    
    public static function replace_wpseo( $info )
    {
        switch ( current_filter() ) {
            case 'wpseo_metadesc':
                if ( isset(self::$metadata['meta_description']) && self::$metadata['meta_description'] ) {
                    return self::$metadata['meta_description'];
                }
            break;
            case 'wpseo_title': 
            case 'wpseo_opengraph_title':
            case 'wpseo_og_og_title':
                if ( isset(self::$metadata['meta_title']) && self::$metadata['meta_title'] ) {
                    return self::$metadata['meta_title'];
                }
            break;
            case 'wpseo_canonical': 
                return $info . self::get_canonical();
            break;

            case 'wpseo_twitter_title': 
                if ( isset(self::$metadata['twitter_title']) && self::$metadata['twitter_title'] ) {
                    return self::$metadata['twitter_title'];
                }
                else if (isset(self::$metadata['meta_title']) && self::$metadata['meta_title']) {
                    return self::$metadata['meta_title'];
                }
            break;
            case 'wpseo_twitter_description': 
                if ( isset(self::$metadata['twitter_description']) && self::$metadata['twitter_description'] ) {
                    return self::$metadata['twitter_description'];
                }
                else if (isset(self::$metadata['meta_description']) && self::$metadata['meta_description']) {
                    return self::$metadata['meta_description'];
                }
            break;
            //case 'wpseo_og_og_image':
            case 'wpseo_twitter_image':
                if ( isset(self::$metadata['twitter_image']) && self::$metadata['twitter_image'] ) {
                    return self::$metadata['twitter_image'];
                }            
                break;
            case 'wpseo_og_description':
                return '';
            break;
        }        
        
        return false;
    }

    
    protected static function get_canonical()
    {
        $QUERY_STRING = rawurldecode(strip_tags(trim($_SERVER['QUERY_STRING'])));
        $QUERY_STRING = str_replace(',', '%2C', $QUERY_STRING);

        $qs = '';
        if ( ! empty($_GET) ) {
            $q = [];
            foreach($_GET as $key => $item) {
                if ( is_array($item) || ! $item ) continue;
                $k       = rawurldecode(strip_tags(trim( $key ) ) );
                $q[ $k ] = rawurldecode(strip_tags(trim( $item )));
            }
            if ( ! empty($q) ) {
                $qs = http_build_query($q);
            }
            $qs = strcmp($QUERY_STRING, $qs) === 0 ? '?'.$qs : '';
        }
    
        return $qs;
    }

    public static function wpseo_opengraph_url()
    {
        $QUERY_STRING = rawurldecode(strip_tags(trim($_SERVER['QUERY_STRING'])));
        $QUERY_STRING = str_replace(',', '%2C', $QUERY_STRING);

        $qs = '';
        if ( ! empty($_GET) ) {
            $q = [];
            foreach($_GET as $key => $item) {
                if ( is_array($item) || ! $item ) continue;
                $k       = rawurldecode(strip_tags(trim( $key ) ) );
                $q[ $k ] = rawurldecode(strip_tags(trim( $item )));
            }
            if ( ! empty($q) ) {
                $qs = http_build_query($q);
            }
            $qs = strcmp($QUERY_STRING, $qs) === 0 ? '?'.$qs : '';
        }
        
        return \WPSEO_Frontend::get_instance()->canonical( false ) . $qs;
    }
}