<?php

namespace Akni\app\Model;

use Akni\app\Helper\Data;
use Akni\app\Model\FilterCat;
/**
 * Class MainAjax
 * @package Akni\app\Model
 */
class MainAjax
{

    private $_core;

    public function __construct($core)
    {
        $this->_core = $core;
        add_action(
            'wp_ajax_getCarInCity',
            [&$this, 'getCarInCity']
        );
        add_action(
            'wp_ajax_nopriv_getCarInCity',
            [&$this, 'getCarInCity']
        );
        add_action(
            'wp_ajax_getPickUpCarLink',
            [&$this, 'getPickUpCarLink']
        );
        add_action(
            'wp_ajax_nopriv_getPickUpCarLink',
            [&$this, 'getPickUpCarLink']
        );
        add_action(
            'wp_ajax_switchCityAction',
            [&$this, 'switchCityAction']
        );
        add_action(
            'wp_ajax_nopriv_switchCityAction',
            [&$this, 'switchCityAction']
        );
        add_action(
            'wp_ajax_changePriceByDate',
            [&$this, 'changePriceByDate']
        );
        add_action(
            'wp_ajax_nopriv_changePriceByDate',
            [&$this, 'changePriceByDate']
        );
        add_action(
            'wp_ajax_carFiltering',
            [&$this, 'carFiltering']
        );
        add_action(
            'wp_ajax_nopriv_carFiltering',
            [&$this, 'carFiltering']
        );

    }

    /**
     * This get count of cars in city.
     * @return bool
     */
    public function getCarInCity()
    {
        if ($_POST['city_id']) {
            $cartCounter = Data::getPostInTermCounter('car', ['cities' => $_POST['city_id']]);
            echo $cartCounter;
            exit;
        } else {
            return false;
        }
    }

    /**
     * This method return pickUpCarLink with filters.
     */
    public function getPickUpCarLink()
    {
        $pickUpCarLink = '';
        if ((int)$_POST['city_id'] !=0 && (int)$_POST['car_class'] !=0) {
            $city = $this->_core->getcities()->getCityByID($_POST['city_id']);
            if (!empty($city) && (int)$city->catalogID !=0) {
                $pickUpCarLink .= get_permalink($city->catalogID);
                if ($_POST['date'] !='') {
                    $pickUpCarLink .= "#date={$_POST['date']}";
                    $pickUpCarLink .= "&class={$_POST['car_class']}";
                } else {
                    $pickUpCarLink .= "#class={$_POST['car_class']}";
                }
            }
        }
        echo $pickUpCarLink;
        exit;
    }

    /**
     * This method return pickUpCarLink with filters.
     */
    public function switchCityAction()
    {
        $pageUrl = '';
        //$search_param = isset($_SESSION['search_param']) ? $_SESSION['search_param'] : '';
        $params = '';
        if ( isset($_COOKIE['f']) && $_COOKIE['f'] ) {
            $params = substr( $_COOKIE['f'], 0, 300 );
            $params = '/filter' . filter_var( rawurldecode( $params ), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH | FILTER_FLAG_STRIP_LOW );
        }


        if ((int)$_POST['city_id']) {
            $newCity = $this->_core->getcities()->getCityByID((int)Data::cleanString($_POST['city_id']));
        } else {
            echo $pageUrl . $params;
            exit;
        }

        if ($_POST['current_page']) {
            $currentPage = (int)Data::cleanString($_POST['current_page']);
            $curCityPages = $this->_core->getCities()->langIDs;

            if ($currentPage !='' && !empty($curCityPages)) {
                if ($pageKey = array_search($currentPage, $curCityPages)) {
                    if($newPageId = $newCity->{$pageKey}) {
                        $pageUrl = get_permalink($newPageId);
                    }
                }
            }
        }

        $_SESSION['current_city'] = $newCity->term_id;

        echo $pageUrl . $params;
        exit;
    }

    /**
     * This method return and days counter.
     */
    public function changePriceByDate()
    {
        $priceHandler = new CarPrice();
        if ((int)$_POST['product_id'] > 0 && (int)$_POST['days'] > 0) {
            $product = get_post((int)$_POST['product_id']);
            $this->_core->getModel()->setResult([$product]);
            $this->_core->getModel()->formattedACF();
            $product = $this->_core->getModel()->getResult()[0];
            if ($product) {
                $priceHandler->setProduct($product);
                $price = $priceHandler->calculatePrice($_POST['days']);
                $price = $priceHandler->calculatePriceOpt( $price, $_POST['options'] );
                if ($price) {
                    echo (string)$price;
                    exit;
                }
            }

        }
    }

    /**
     * This method return and days counter.
     *
     * @return void
     */
    public function carFiltering()
    {
        global $core;

        FilterCat::init_ajax_params();
        $context = FilterCat::get_context_data();
        
        $specialFilters = [];

        $config = $core->getConfig();
        $carsConfig = $config->getConfig('show', 'show')['cars'];
        $taxQuery['relation']  = 'AND';

        if ((int)$_SESSION['current_city']) {
            $taxQuery[] = [
                'taxonomy' => 'cities',
                'field' => 'id',
                'terms' => (int)$_SESSION['current_city'],
            ];
            $carsConfig['tax_query'] = $taxQuery;
        }


        $filters = $_POST['data']['params'];

        if (!empty($filters)) {
            if (isset($filters['class']) && $filters['class'] && $filters['class'][1] !='') {
                $carsConfig = $this->_setCarClass($carsConfig, $filters['class'][1]);
                unset ($filters['class']);
            }

            if (isset($filters['price']) && $filters['price']) {
                if ($filters['price'][1] !=''){
                    $filterPrice = explode(',', $filters['price'][1]);
                    if (is_array($filterPrice) && count($filterPrice == 2)) {
                        $specialFilters['price'] = $filterPrice;
                    }
                }
                unset ($filters['price']);
            }

            if (isset($filters['date'])) {
                //this is no filter
                unset ($filters['date']);
            }

            if (!empty($filters)) {
                $carsConfig = $this->_setFilters($carsConfig, $filters);
            }
        }

        $cars = $this->_getFilteredCars($carsConfig, $specialFilters);

        if (isset($_POST['data']['count']) && ! $_POST['data']['count']) {
            echo json_encode(count($cars));
            if ($this->is_ajax() ) die;
        }
        else if ( isset($_POST['data']) )  {
            $context['content'] = $this->_renderCatalogCars($cars);
            echo json_encode( $context );
            die;
        } 
    }

    // 25.07.2018
    public function is_ajax() 
    {
        if( ! empty( $_SERVER[ 'HTTP_X_REQUESTED_WITH' ] ) &&
              strtolower( $_SERVER[ 'HTTP_X_REQUESTED_WITH' ]) == 'xmlhttprequest' ) {
            return true;
        }
        return false;
    }

    /**
     * Method to set filters
     *
     * @param $carsConfig
     * @param $filters
     * @return array
     */
    private function _setFilters($carsConfig, $filters)
    {
        $metaQuery['relation']  = 'AND';
        foreach ($filters as $filter) {
            if ( isset($filter[0]) && isset($filter[1]) && $filter[0] && $filter[1] !=='') {
                $filterValuesArr = explode(',', $filter[1]);
                if (count($filterValuesArr) > 1) {
                    $metaQuery[] = [
                        'key'	 	=> $filter[0],
                        'value'	  	=> $filterValuesArr,
                        'compare' 	=> 'IN',
                    ];
                } else {
                    $metaQuery[] = [
                        'key'	 	=> $filter[0],
                        'value'	  	=> $filter[1],
                        'compare' 	=> '=',
                    ];
                }

            }

        }
        $carsConfig['meta_query'] = $metaQuery;
        return $carsConfig;
    }

    /**
     * Method to set car class
     *
     * @param $carsConfig
     * @param $classes
     * @return mixed
     */
    private function _setCarClass($carsConfig, $classes)
    {


        $taxQuery[] = [
            'taxonomy' => 'class',
            //'field' => 'id', // 02.08.2018
            'field' => 'slug',
            'terms' => explode(',', $classes)
        ];
        $carsConfig['tax_query'][]= $taxQuery;

        return $carsConfig;
    }

    /**
     * Method to render catalog block
     * @param $cars
     * @return array
     */
    private function _renderCatalog($cars)
    {
        $catalog = [];
        $catalog ['content'] = $this->_renderCatalogCars($cars);
        return $catalog;

    }

    /**
     * Method to get cars
     *
     * @param $cars
     * @return bool|string
     */
    private function _renderCatalogCars($cars)
    {
        global $core;

        $context = $core->get_context();
        $context['cars']['cars'] = array_chunk($cars, 9);

        //  <span class="days">{{ lang.for_day }}</span>
        if (isset($context['lang']['for_day_png'])) {
            $context['lang']['day_src'] = THEME_URI . '/public/images/' . $context['lang']['for_day_png'] . '.png';
        }        
        return $core->renderWithoutOutput('_cars.twig', $context);
    }

    /**
     * Method to get filtered cars
     *
     * @param $carsConfig
     * @param array $specialFilters
     * @return array
     */
    private function _getFilteredCars($carsConfig, array $specialFilters = [])
    {
        global $core;

        $model  = $core->getModel();
        $priceHandler = new CarPrice();
        $model->setArgs($carsConfig);
        $model->setPosts();
        $model->setMainThumbnailUrls([300, 300]);
        $model->setPostUrls();
        $model->formattedACF();
        $cars = $model->getResult();

        if (!empty($specialFilters)) {
            if(!empty($specialFilters['price'])) {
                $min_price = min($specialFilters['price']);
                $max_price = max($specialFilters['price']);
            }
        }

        if (!empty($cars)) {
            foreach($cars as $key=>$car) {
                $priceHandler->setProduct($car);

                $cars[$key]->regularPrice = $priceHandler->getRegularPrice();
                $cars[$key]->specialPrice = $priceHandler->getSpecialPrice();
                $cars[$key]->calculatedPrice = $priceHandler->calculatePrice();

                /**
                 * new price table
                 */
                $cars[$key]->priceTable = $priceHandler->getPriceTable();
                $cars[$key]->deliveryPrice = $priceHandler->getDeliveryPrice();

                $carClass = Data::getMainTerm($car->ID, 'class');
                if (!$carClass->name) {
                    unset($cars[$key]);
                    continue;
                }
                $dealCities = get_the_terms($car->ID, 'cities');
                $orderCitiesArr = [];

                foreach ($dealCities as $city ){
                    $orderCitiesArr[] = $city->name;
                }

                $orderCities = implode(',', $orderCitiesArr);
                $cars[$key]->orderCities = $orderCities;
                $cars[$key]->class_label = $carClass->name;

                if (isset($min_price) && $min_price != 0 && isset($max_price) && $max_price !=0) {
                    if ($cars[$key]->calculatedPrice < $min_price || $cars[$key]->calculatedPrice > $max_price) {
                        unset ($cars[$key]);
                    }
                }
            }
        }
        return $cars;
    }
}