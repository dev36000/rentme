<?php
namespace Akni\app\Model;


class Cities
{
    private $_currentCityID;

    private $_currentCityInfo;

    private $cities;

    public  $langUrls;

    public  $langIDs;

    /**
     * Cities constructor.
     */
    public function __construct()
    {
        session_start();
        $this->cities = $this->getCities();
        $this->setCurrentCityID();
        $this->setLangIDs();
        $this->setLangUrls();
        $this->setCurrentCityInfo();
    }

    public function setLangUrls()
    {
        $urls = [];

        if (!empty($this->langIDs)) {
            $urls['home'] = get_permalink($this->langIDs['homeID']);
            $urls['contact'] = get_permalink($this->langIDs['contactID']);
            $urls['catalog'] = get_permalink($this->langIDs['catalogID']);
        }
        $this->langUrls = $urls;
    }

    public function setLangIDs()
    {
        $ids = [];
        if (!empty($this->cities) && $this->_currentCityID !='') {
            foreach($this->cities as $city) {
                if($city->term_id == $this->_currentCityID) {
                    $ids['homeID'] = $city->homeID;
                    $ids['contactID'] = $city->contactID;
                    $ids['catalogID'] = $city->catalogID;
                }
            }
        }
        $this->langIDs = $ids;
    }

    public function setCurrentCityID()
    {
        $uri = strtolower($_SERVER['REQUEST_URI']);

        #This is hard fix. Will updated, when find better solution.
        $defaultUrls = ['/ua/','/en/','/ru/','/'];
        if (in_array($uri , $defaultUrls)) {
            if($frontPageId = get_option( 'page_on_front' )) {
                if(!empty($this->cities)) {
                    foreach ($this->cities as  $city) {
                        if ($frontPageId == $city->homeID) {
                            $this->_currentCityID = $city->term_id;
                            $_SESSION['current_city'] = $city->term_id;
                        }
                    }
                }
            }
            return 'Hard fix for home page. Update me!';
        }
        ###########################################################

        if(!empty($this->cities)) {
            foreach ($this->cities as $key=>$city) {
                $cities['pages'] = [
                  'home' => get_post($city->homeID)->post_name,
//                  'catalog' => get_post($city->catalogID)->post_name,
//                  'contact' => get_post($city->contactID)->post_name
                ];
                if (
                    stripos($uri, strtolower($cities['pages']['home'])) !== false ||
//                    stripos($uri, strtolower($cities['pages']['catalog'])) !== false ||
//                    stripos($uri, strtolower($cities['pages']['contact'])) !== false ||
                    stripos($uri, strtolower($city->slug)) !== false
                    ) {
                    $this->_currentCityID = $city->term_id;
                    $_SESSION['current_city'] = $city->term_id;
                }
            }

            if (!isset($_SESSION['current_city'])) {
                if(!$this->_currentCityID) {
                    if($frontPageId = get_option( 'page_on_front' )) {
                        $page = get_post($frontPageId);
                        if ($term = wp_get_post_terms( $page->ID, 'cities')[0]){
                            $this->_currentCityID = $term->term_id;
                            $_SESSION['current_city'] = $this->_currentCityID;
                        }
                    }
                }
            } else {
                $this->_currentCityID = $_SESSION['current_city'];
            }
        }
        return true;
    }

    public function getCurrentCityID()
    {
        return $this->_currentCityID;

    }
    public function getCities()
    {
        $cities = get_terms('cities');
        $sortedCities = [];

        if (!empty($cities)) {
            foreach ($cities as &$city) {
                $city->homeID = get_field('home_id', $city);
                $city->contactID = get_field('contact_id', $city);
                $city->catalogID = get_field('catalog_id', $city);
                $sortedCities[$city->term_id] = $city;
            }

        }
        return $sortedCities;
    }

    /**
     * Method to generate switcher.
     * @return string $html.
     */
    public function generateSwitcher()
    {
        $html ='';

        if (!empty($this->cities)) {
            $html .= "<select class='city-switcher' id='city_switcher'>";
            foreach ($this->cities as $city) {
                $selected = '';
                if ($this->_currentCityID == $city->term_id) {
                    $selected = 'selected="selected"';
                }
                $html .="<option class='select-item' $selected value='{$city->term_id}'>";
                $html .="$city->name";
                $html .="</option>";
            }
            $html .="</select>";
        }
        return $html;
    }

    /**
     * Method to create switcher
     * @param bool $showOnce
     * @return string
     */
    public function generateFirstTimeSwitcher($showOnce = true)
    {
        if (!$_SESSION['first_switch'] || $showOnce == false) {
            $html ='';
            if (!empty($this->cities)) {
                $html .= "<div  id='first_time_switcher'>";
                foreach ($this->cities as $city) {
                    $selected = '';
                    if ($this->_currentCityID == $city->term_id) {
                        $selected = 'selected="selected"';
                    }
                    $html .="<span class='switcher-item' $selected data-value='{$city->term_id}'>";
                    $html .="$city->name";
                    $html .="</span>";
                }
                $html .="</div>";
            }
            $_SESSION['first_switch'] = 1;
            return $html;
        }
        return '';
    }

    /**
     * Method to create catalog time switcher.
     */
    public function generateCatalogSwitcher()
    {
            $html ='';
            if (!empty($this->cities)) {
                $html .= "<div  id='catalog_switcher'>";
                foreach ($this->cities as $city) {
                    $checked = '';
                    if ($this->_currentCityID == $city->term_id) {
                        $checked = 'checked="checked"';
                    }
                    $html .="<label>";
                    $html .="<input type ='radio' class='switcher-item' name='catalog_city' value='{$city->term_id}' $checked data-value='{$city->term_id}'/>";
                    $html .="<span>{$city->name}</span>";
                    $html .="</label>";
                }
                $html .="</div>";
            }
            return $html;
    }

    /**
     * Method to get  city by id.
     */
    public function getCityByID($key)
    {
        if ($this->cities[$key]){
            return $this->cities[$key];
        }
        return false;
    }

    /**
     * Method to get info about current city.
     */
    public function getCurrentCityInfo()
    {
        return $this->_currentCityInfo;
    }

    private function setCurrentCityInfo()
    {
        $info = [];
        if ($this->_currentCityID !='' && !empty($this->cities)) {
            $currentCity = $this->getCityByID($this->_currentCityID);
            if (!empty($currentCity)) {
               $info['name'] = $currentCity->name;
               $info['address'] = get_field('address', $currentCity);
               // 12.04.2018
               if ( ! empty( $phones = get_field('phone', $currentCity) ) ) {
                    if (strpos($phones, ',') === false
                        && is_array($a_phones = explode(PHP_EOL, preg_replace('|\(.*?\)|', '', $phones) ) )
                    ) {
                        
                        foreach($a_phones as $item) {
                              $p = array_map('trim', explode('|', $item));
                              
                              $info['phones'][] = 
                                [
                                    'phone' => isset($p[0]) ? $p[0] : '',
                                    'href'  => isset($p[1]) ? $p[1] : '', 
                                    'icon'  => isset($p[2]) ? $p[2] : ''
                                ];
                        }
                    }
                    else {
                        $info['phones'] = array_map('trim', explode(',', preg_replace('|\(.*?\)|', '', $phones) ) );
                    }

               }
            }
        }
        $this->_currentCityInfo = $info;
    }
}