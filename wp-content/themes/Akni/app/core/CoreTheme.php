<?php

namespace Akni\app\core;

use Akni\app\Model\Construct;

use Akni\app\Model\Model;
use Akni\app\Model\Cities;
use Akni\app\Helper\Data;
use Timber\Loader;
use Timber\Timber;
class CoreTheme extends Timber
{
    /**
     * @var Config load configurations method
     */
    private $_config;

    /**
     * @var Construct register parts of wp
     */
    private $_construct;

    /**
     * Model usages
     */
    public $model;


    /**
     * @var $_cities object
     */
    private $_cities;

    /**
     * Path to current theme
     * @var $themeUri .
     */
    public $themeUri;
     /**
     * Core constructor.
     */
    public function __construct()
    {
        $this->themeUri = get_template_directory_uri();
        add_action('after_setup_theme', [$this, 'setUp']);
        add_filter(
            'acf/fields/google_map/api', [$this, 'setMapApiKey']
        );
        add_action(
            'admin_enqueue_scripts', [&$this, 'addAdminScripts']
        );

        parent::__construct();
    }

    /**
     * @param $name
     * @param $arguments
     * @return mixed
     * This method need for use $this,
     * for default php functions.
     */
    public function __call($name, $arguments)
    {
        if (function_exists($name)) {
            return call_user_func_array($name, $arguments);
        }
        return false;
    }

    /**
     * @return Model
     * Model getter
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Ciites getter;
     */
    public function getCities()
    {
        return $this->_cities;
    }

    /**
     * @return Config
     * Config getter
     */
    public function getConfig()
    {
        return $this->_config;
    }

    /**
     * Setup all main classes
     */
    public function setUp()
    {
        $this->_config = new Config();
        $this->_construct = new Construct();
        $this->model = new Model();
        $this->_cities = new Cities();

        $this->add_filter('timber_context', [$this, 'addToContext']);

        $this->add_filter('timber_context', [$this, 'addToFooter']);
    }

    /**
     * @param $data
     * @return mixed
     * return all variables added to context
     */
    public function addToContext($data)
    {

        #currentPage
        $data['currentPageId'] = get_the_ID();

        #lang
        $data['lang'] = [];
        $langConfig = $this->_config->getConfig('lang');
        foreach ($langConfig as $config => $value) {
            if (!is_array($value)) {
                $data['lang'][$config] = __($value);

            } else {
                foreach ($value as &$val) {
                    $val = __($val);
                }
                $data['lang'][$config] = $value;
            }
        }

        #site urls
        $data['site_url'] = $this->get_site_url();
        $data['ajax_url'] = $this->get_site_url() . "/wp-admin/admin-ajax.php";

        #random number in string format
        $data['rand'] = (string)rand();

        #header menu
        /*$headerMenuLocation = get_nav_menu_locations()['primary-header-menu'];
        if ($headerMenuLocation !== null) {
            $data['primary_header_menu'] = wp_get_nav_menu_items(
                $headerMenuLocation
            );
        }*/

        #theme_options
        $data['theme_options'] = (array)get_option('ice-theme-settings');

        #forms
        $data['forms'] = $this->addMainContactForms($data['lang']);

        #Cities
        $data['current_cityID'] = $this->_cities->getCurrentCityID();
        $data['city_urls'] = $this->_cities->langUrls;
        $data['city_info'] = $this->_cities->getCurrentCityInfo();

        if($data['theme_options']['switcher'] !='disable') {
            $data['first_time_switcher'] = $this->_cities->generateFirstTimeSwitcher();
            $data['contact_switcher'] = $this->_cities->generateFirstTimeSwitcher(false);
            $data['city_switcher'] = $this->_cities->generateSwitcher();
            $data['header_switcher'] = $this->_cities->generateHeaderSwitcher();
            $data['catalog_switcher'] = $this->_cities->generateCatalogSwitcher();
        }


        #menus
        $data['menus'] = $this->setMenus();

        #language
        $data['language_switcher'] = $this->langugeSwitch();

        #theme url
        $data['theme_url'] = $this->themeUri . '/public/images/';
        return $data;
    }

    /**
     * @param $data
     * @return mixed
     * return all variables added to footer
     */
    public function AddToFooter($data){

        $col_4 = get_field('section_4','options');
        $data['footer_col_4'] = $col_4;
        return $data;
    }

    /**
     * This format date.
     * @param $date
     * @return string
     */
    public function specialDateFormat($date)
    {
        $d = date('d', strtotime($date));
        $m = date('m', strtotime($date));
        $y = date('Y', strtotime($date));
        $month = __($this->_config->getConfig(
            "date",
            "style_config"
        )
        ['months'][$m]);
        $formated_date = $d . ' ' . mb_strtolower($month, 'UTF-8') . ' ' . $y;
        return $formated_date;
    }

    /**
     * @param $api
     * @return mixed
     * This set google map api
     */
    public function setMapApiKey($api)
    {
        $api['key'] = 'AIzaSyDlz7f8qQwqDy9wl8IRIZ58NiYgXTrqBTk';
        return $api;
    }


    /**
     * This function add theme.
     */
    public function addAdminScripts()
    {
        wp_enqueue_script(
            'acf_translate', $this->themeUri . '/public/js/acf_translate.js', ['jquery']
        );
    }

    /**
     * This function add main forms.
     * @param array $lang
     * @return array
     */
    public function addMainContactForms(array $lang)
    {
        $forms = [];
        #callback
        $callback_data = [
            'content' => [
                'type' => 'Callback',
                'formTitle' => $lang['application_title'],
                'formDescription' => '',
                'thankYouText' => $lang['thank_you_for_callback_text'],
                'submitText' => $lang['send_text'],
            ],
            'name' => [
                'show' => 1,
                'order' => 1,
                'placeholder' => $lang['name_text'],
                'class' => 'form-field name',
                'error' => $lang['name_error_text']
            ],
            'phone' => [
                'show' => 1,
                'order' => 2,
                'placeholder' => $lang['phone_text'],
                'class' => 'form-field tel',
                'error' => $lang['phone_error_text'],
            ],
            'email' => [
                'show' => 0,
            ],
        ];
        $callback_data2 = $callback_data;

        $callback_data2['content']['formTitle'] = $lang['callback_title'];

        $callback_params1 = serialize($callback_data);
        $callback_params2 = serialize($callback_data2);

        $order_params = serialize([
            'content' => [
                'type' => 'order',
                'formTitle' => $lang['order_title'],
                'formDescription' => '',
                'thankYouText' => $lang['thank_you_for_callback_text'],
                'submitText' => $lang['send_text'],
            ],
            'name' => [
                'placeholder' => $lang['name_and_lastname_text'],
                'class' => 'form-field name',
                'error' => $lang['name_and_lastname_error_text']
            ],
            'phone' => [
                'placeholder' => $lang['phone_placeholder'],
                'class' => 'form-field tel',
                'error' => $lang['phone_error_text'],
            ],
            'city' => [
                'placeholder' =>$lang['city_text'],
                'class' => 'form-field city',
            ],
            'date_from' => [
                'placeholder' => $lang['when_text'],
                'class' => 'form-field date_from',
                'error' => $lang['date_from_error_text'],
            ],
            'date_to' => [
                'placeholder' => $lang['return_text'],
                'class' => 'form-field date_to',
                'error' => $lang['date_to_error_text'],
            ],
            'delivery' => [
                'placeholder' => $lang['delivery_title_text'],
                'class' => 'form-field delivery',
            ],
            'sum' => [
                'placeholder' => $lang['total_text'],
                'class' => 'form-field delivery',
            ],
            'days' => [
                'placeholder' => $lang['days_text'],

            ],

        ]);

        $forms['callback_footer'] = do_shortcode("[callback params='{$callback_params2}']");
        $forms['callback_main'] = do_shortcode("[callback params='{$callback_params1}']");
        $forms['order'] = do_shortcode("[special_callback params='{$order_params}']");
        return $forms;
    }

    /**
     *This is temporary solution of lang switcher.
     * Rewrite it and update as soon as possible.
     * @return string
     */
    private final function langugeSwitch()
    {
        global $q_config;
        $switcher ='';
        $id = 'switcher';
        $id .= '-chooser';
        if(is_404()) $url = get_option('home'); else $url = '';
        $switcher .= PHP_EOL.'<ul class="language-chooser language-chooser-dropdown qtranxs_language_chooser" id="'.$id.'">'.PHP_EOL;
        foreach(qtranxf_getSortedLanguages() as $language) {
            $alt = $q_config['language_name'][$language].' ('.$language.')';
            $classes = array('lang-'.$language);
            if($language == $q_config['language']) $classes[] = 'active';
            $switcher .= '<li class="'. implode(' ', $classes) .'"><a href="'.qtranxf_convertURL($url, $language, false, true).'"';

            if(!empty($q_config['locale_html'][$language])){
                $lang = $q_config['locale_html'][$language];
            }else{
                $lang = $language;
            }

            $switcher .=  ' hreflang="'.$lang.'"';
			$switcher .=  ' title="'.$alt.'"';
            $switcher .=  ' class="qtranxs_css qtranxs_css_'.$language.'"';
            $switcher .=  '>';
            $switcher .=  '>'.$q_config['language_name'][$language].'</span>';
            $switcher .=  '</a></li>'.PHP_EOL;
        }

        $switcher .=  '<script type="text/javascript">'.PHP_EOL.'// <![CDATA['.PHP_EOL;
        $switcher .=  "var lc = document.getElementById('".$id."');".PHP_EOL;
        $switcher .=  "var s = document.createElement('select');".PHP_EOL;
        $switcher .= "s.id = 'qtranxs_select_".$id."';".PHP_EOL;
        $switcher .=  "lc.parentNode.insertBefore(s,lc);".PHP_EOL;
        foreach(qtranxf_getSortedLanguages() as $language) {
            $switcher .=  qtranxf_insertDropDownElement($language, qtranxf_convertURL($url, $language, false, true), $id);
        }
        $switcher .= "s.onchange = function() { document.location.href = this.value;}".PHP_EOL;
        $switcher .=  "lc.style.display='none';".PHP_EOL;
        $switcher .=  '// ]]>'.PHP_EOL.'</script>'.PHP_EOL;


        $switcher .=  '</ul><div class="qtranxs_widget_end"></div>'.PHP_EOL;
        return $switcher;
    }

    /**
     * Method for adding new menu to context.
     */
    private function setMenus() {
        $menus = [];
        $menu_config = $this->_config->getConfig('menu_config','style_config');

        $menus['primary_header_menu'] = strip_tags(wp_nav_menu(
            $menu_config['primary_header_menu']
        ), "<a>");

        $menus['useful_links_footer'] = strip_tags(wp_nav_menu(
            $menu_config['useful_links_footer']
        ), "<a>");

        $menus['about_company_footer'] = strip_tags(wp_nav_menu(
            $menu_config['about_company_footer']
        ), "<a>");

        return $menus;
    }

    /**
     * Render function.
     * @api
     * @param array   $filenames
     * @param array   $data
     * @param bool    $expires
     * @param string  $cache_mode
     * @return bool|string
     */
    public static function renderWithoutOutput( $filenames, $data = array(), $expires = false, $cache_mode = Loader::CACHE_USE_DEFAULT ) {
        $output = self::fetch($filenames, $data, $expires, $cache_mode);
        return $output;
    }
}