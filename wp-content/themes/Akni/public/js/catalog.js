requirejs([
    'jquery',
    'jquery-seo2',
    'jquery-ui.min',
    'jquery.cookie.min'
], function ($) {
    'use strict';
    var basepath = (location.pathname.match(/^(.*)\/filter/)||[])[1] || '';
    basepath = basepath ? basepath + '/' : '';
    var filters = {
        search_params : {},
        cars:'#products_list_block',
        sliderRange: '#slider-range',
        amount: '#amount',
        resultFrom: '.result_from',
        resultTo: '.result_to',
        resetBtn: '.reset_filters',
        // allow
        filter_context: [ '.page_title', 'title', '.seo_block', '.annotations' ],
        btnClicker : null,
        root_path  : basepath || location.pathname,
        ishistori_api :!!(window.history && history.pushState),

        init: function ()
        {
            this.sliderInit();
            //this.setFilter();
            this.getSearchParams();
            this.checkedSearchParams();
            this.changeFilter();
            this.filterReset();
            this.mobileFilterAction();
        },


        filterReset: function()
        {
            var _this = this;

            $(this.resetBtn).on('click', function(){
                _this.search_params = {};
                 $('.filters input').prop("checked", false);
                _this.sliderInit();
                _this.filtering(true);
                $('#show-more-filters').removeClass('move').addClass('hide');
                $(this).hide();
                $('.filters .sel li.def a').click();
            });
        },

        clearParams: function()
        {
            this.search_params = {};
            $('.filters input').prop("checked", false);
        },

        sliderInit :function()
        {
            var _this = this;

            var thisSearch = $('input.price');

            $(_this.resultFrom + ' .value input, ' + _this.resultTo + ' .value input').bind( 'input', function(){
                setTimeout(function() {
                    var s = $( _this.sliderRange).slider();
                    s.slider('values',[$(_this.resultFrom + ' .value input').val(), $(_this.resultTo + ' .value input').val()]);
                    s.trigger('slide',{ ui: $('.ui-slider-handle', s), value: 10 });

                    var thisVal = $(_this.resultFrom + ' .value input').val() + ',' + $(_this.resultTo + ' .value input').val();
                    thisSearch.val(thisVal);
                    //_this.addHash($(  _this.amount ), thisVal);
                    //_this.setHash();
                    // _this.addSearch($(this));
                    _this.addSearch(thisSearch,thisVal);
                    _this.filtering();
                    console.log(_this);
                    $('#show-more-filters').click();
                }, 100);

            });

            $( _this.sliderRange).slider({
                range: true,
                min: $(_this.amount).data('min'),
                max: $(_this.amount).data('max'),
                values: [ $(_this.amount).data('min'), $(_this.amount).data('max') ],
                slide: function( event, ui ) {
                    $( _this.amount).val(  window.baseObj.currency + ui.values[0] +
                        " - " +
                        window.baseObj.currency + ui.values[1] );

                    $(_this.resultFrom + ' .value .currency').html(window.baseObj.currency);
                    $(_this.resultFrom + ' .value input').val(ui.values[0]);

                    $(_this.resultTo + ' .value .currency').html(window.baseObj.currency);
                    $(_this.resultTo + ' .value input').val(ui.values[1]);

                },

                stop: function( event, ui ) {

                    var thisVal = ui.values[0] + ',' + ui.values[1];
                    thisSearch.val(thisVal);
                   // _this.addHash($(  _this.amount ), thisVal);
                  //  _this.setHash();
                    _this.addSearch(thisSearch,thisVal);
                    _this.filtering();
                    $('#show-more-filters').click();
                }
            });

            $(_this.amount).val(
                window.baseObj.currency + $( _this.sliderRange).slider( "values", 0 ) +
                " - " +
                window.baseObj.currency + $( _this.sliderRange).slider( "values", 1 )
            );
            $(this.resultFrom + ' .value .currency').html(window.baseObj.currency);
            $(this.resultFrom + ' .value input').val($( _this.sliderRange).slider( "values", 0 ));

            $(this.resultTo + ' .value .currency').html(window.baseObj.currency);
            $(this.resultTo + ' .value input').val($( _this.sliderRange).slider( "values", 1 ));
        },

        getFiletr : function () {
            return '' + (jQuery.cookie('f') || '');
        },

        /**
         *             var filter = this.getSearch();
         */
        saveHistory: function(filter)
        {
            if (filter) {
                var pathname = this.root_path + 'filter' + filter;
                history.pushState({page: ''+pathname}, null, location.origin + pathname );
            }
        },

        /**
         * получаем параметры поиска
         */
        getSearchParams : function (pathname) {
            var _this = this, isfilter = false;
            var struri = _this.getFilterURI( pathname || location.pathname );
            if ( struri ) {
                var key, value;
                _this.search_params = {};
                var a_struri = struri.split('/');
                for (var i = 0; i < a_struri.length; i++) {
                    var p = a_struri[i].split('-');
                    key = p.length>1 ? ''+p.shift() : '';
                    for (var j = 0; j < p.length; j++) {
                        if (''+p[j]) {
                            value = ((''+p[j]).match(/[a-zA-Z0-9]+/)||[])[0];
                            if (value) {p[j] = value;}
                        }
                    }
                    if (key) {
                        _this.search_params[ key ] = [key, p.join(',')];
                        isfilter = true;
                    }
                }
            }
            isfilter ? jQuery.cookie('f', '/' + struri, { expires: 1, path: '/' }) : jQuery.cookie('f', null, { expires: 1, path: '/' });

            return isfilter;
        },

        getFilterURI: function(pathname) {
            var path   = pathname || location.pathname;
            var _filter = (path.match(/\/filter\/(.*)/)||[])[1]||'';

            return _filter ?_filter : '';
        },

        filtering :function (filteringTrue) {
            var _this = this;
            // !window.products.isMobile ||
            if (filteringTrue) {
                var formData = {
                    'action':'carFiltering',
                    'data': {'params':_this.search_params, uri: _this.getSearch(), pid: $("#ajax_params").data('pid')||'' }
                };


                if ( 'yes' === $("#ajax_params").data('filter') ) {
                    var url = $("#ajax_params").data('citi-url') || '';
                    location.href = url + 'filter'+ _this.getSearch();
                }
                else if ( _this.ishistori_api ) {
                    window.baseObj.ajaxAction(formData, this._success.bind(_this));
                }
                else {
                    location.pathname = _this.root_path + 'filter'+ _this.getSearch();
                }
            }
            else {
                var formData = {
                    'action':'carFiltering',
                    'disableLoader': true,
                    'data': {'params':_this.search_params, 'count':1, uri: _this.getSearch(), pid: $("#ajax_params").data('pid')||'' }
                };
                window.baseObj.ajaxAction(formData, this._countSuccess.bind(_this));
            }
        },


        /**
         * инициализация значений элементов
         */
        checkedSearchParams: function () {
            var _this = this;
            for (var elem in _this.search_params) {
                if(_this.search_params[elem][1] != undefined && elem != 'price') {
                    var val = _this.search_params[elem][1].replace('%2C', ',').split(',');
              /*     console.log(val);*/

                    for(var i = 0; i < val.length; i++){
                        $('.filters input[name="' + elem + '"][value="' + val[i] + '"]').prop('checked', 'checked');
                    }
                }
            }
        },


        /**
         * Добавляем параметры в запрос по событию change фильтра
         */
        addSearch : function(thisInput , slider) {
            var _this = this;

            var str = '';
            $('.filters input[name="' + thisInput.attr('name') + '"]:checked').each(function(i){
                if(i != 0)
                    str += ',';

                str += $(this).val();
            });

            if (slider !== undefined) {

                str = slider;
                console.log(slider);
            }

            var attrName = _this.search_params[thisInput.attr('name')];

            if (attrName) {
                attrName[1] = str;
            } else{
                _this.search_params[thisInput.attr('name')] = [thisInput.attr('name'), str];
            }
        },


        getSearch: function()
        {
            var _this = this, _search_uri='';
            for(var filter in _this.search_params){
                var name = ''+_this.search_params[filter][0];
                var val = ''+_this.search_params[filter][1];

                if (name == undefined || name =='' || val  == undefined || val =='') {
                    continue;
                }

                _search_uri += '/' + name + '-' + (val.replace(/,/g,'-'));
            }

            $(this.resetBtn).show();

            return _search_uri;//  || '/';

        },

        changeFilter: function ()
        {
            var _this = this;

            $('.filters input').change(function(){
                _this.btnClicker = $(this);
                _this.addSearch($(this));
                _this.filtering();
            });

            $('.filters .ev_filter').on('click', function(ev) {
                ev.preventDefault();

                var input = $(ev.target).closest('label').find('input');
                if (input) {
                     input.prop('checked', ! input.is(':checked') );
                    _this.btnClicker = input;
                    _this.addSearch(input);
                    _this.filtering();
                    $('#show-more-filters').click();
                }

                return false;
            });
        },

        _success: function(success)
        {
            if (success !="") {
                success = JSON.parse(success);
                if (success['content']) {
                    $(filters.cars).html(success['content']);

                    for (var i=0; i<this.filter_context.length; i++) {
                        if ($(this.filter_context[i]).length && success[this.filter_context[i]]) {
                            $(this.filter_context[i]).html(''+success[this.filter_context[i]]);
                        }
                    }

                    window.products.init();
                    window.products.startScrollTop = true;

                    var filter = this.getSearch();
                    var pathname = this.root_path + 'filter' + filter;
                    history.pushState({page: ''+pathname}, null, location.origin + pathname );
                    jQuery.cookie('f', filter, { expires: 1, path: '/' });
                }
            }
        },
        _countSuccess: function(success)
        {
            if (success !="") {
                $('#show-more-filters').find('#count').html('(' +success+ ')' );
                for (var i=0; i<this.filter_context.length; i++) {
                    if ($(this.filter_context[i]).length && success[this.filter_context[i]]) {
                        $(this.filter_context[i]).html(''+success[this.filter_context[i]]);
                    }
                }
                filters.pickUpBtnPosition($(filters.btnClicker).parent());
            }
        },

        mobileFilterAction: function() {
            var _this = this;
            $(document).on('click touchstart', '#show-more-filters', function(event){
                event.preventDefault();
                _this.filtering(true);
                $('#show-more-filters').removeClass('move').addClass('hide');
               /* if (window.products.isMobile) {
                    window.baseObj.fixBody($(mobileFilter.wrapp), false);
                    mobileFilter._hide();
                }*/
            });

            $('.filters_block_wrapper').bind('scroll', function(){
                if(!$('#show-more-filters').hasClass('hide')){
                    var top = _this.btnClicker.offset().top - 4;
                    $('#show-more-filters').removeClass('move').css('top', top + 'px');
                }
            });
        },
        pickUpBtnPosition: function(elem){
            if(!elem.length)
                return;

            var top = elem.position().top - 4,
                left = elem.find('span').outerWidth(true) + 55;

            if(window.products.isMobile && left > ($('.filters_block').width() - 100)){
                left = $('.filters_block').width() - 100;
            }

            if($('#show-more-filters').hasClass('hide')){
                $('#show-more-filters').removeClass('hide');
            }else{
                $('#show-more-filters').addClass('move');
            }

            this.btnClicker = elem;

            $('#show-more-filters').css({'top': top + 'px', 'left': left + 'px', position:'absolute'});
        }
    };

    window.products = {
        wrapp: '.products_list_block',
        item: '.products_item',
        products: '.products_list_block .products_list',
        moreLink: 'more_cars_link',
        countOfVisible: 9,
        isMobile: null,
        startScrollTop: false,
        params: {
            margin: 10,
            nav: true,
            pagination: true,
            loop: false,
            autoHeight: true,
            items: 1,

            onChanged: function(){
                if(window.products.startScrollTop == true){
                    var top = $('.filters_block_wrapper .filters').offset().top;
                  /*  $('html, body').animate({scrollTop: top}, 300, function() {
                        //jQuery.fn.seo('setpos');
                    });*/
                }
            }
        },

        init: function(){
            var _this = this;

            this.isMobile = null;
            $('.products_list .products_item .image-holder .image').setSameHeight();
            $('.products_list_block .products_item .price').setSameHeight();
            $('.products_list_block .products_item .title').setSameHeight();
            $('.products_list_block .products_item .price_grid').setSameHeightInLine([{minWidth: 1150, count: 3}, {minWidth: 820, count: 2}]);
            _this._activeDisableMobile();
            $(window).on('resize', function(){
                _this._activeDisableMobile();
            });
        },
        _activeDisableMobile: function(){
            if(window.innerWidth > 640){
                if(this.isMobile == true || this.isMobile == null){
                    this._disable();

                    this.isMobile = false;
                }
            }else{
                if(this.isMobile == false || this.isMobile == null){
                    this._active();

                    this.isMobile = true;
                }
            }

        },

        _active: function(){
            var _this = this;

            this.disableMobileSlider();

            if($(this.item).length > this.countOfVisible){
                $(this.wrapp).append('<span class="' + this.moreLink + '">'+baseObj.more_cars_text +'</span>');
            }

            _this.hideProducts();

            $('.' + this.moreLink).bind('click', function(){
                _this.showProducts();
            });
        },
        _disable: function(){
            this.hideProducts('all');
            $('.' + this.moreLink).remove();
            this.activeMobileSlider();
        },
        hideProducts: function(all){
            //console.log('hideProducts');
            var _this = this;

            if(all !== undefined){
                $(this.wrapp + ' ' + this.item).removeClass('hide');
                return;
            }

            $(this.wrapp + ' ' + this.item).each(function(i){
                if(i >= _this.countOfVisible){
                    $(this).addClass('hide');
                }
            });
        },
        showProducts: function(){
            var _this = this;

            $(this.wrapp + ' ' + this.item + '.hide').each(function(i){
                if(i < _this.countOfVisible){
                    $(this).removeClass('hide');
                }
            });

            if($(this.wrapp + ' ' + this.item + '.hide').length == 0)
                $('.' + this.moreLink).remove();

           // jQuery.fn.seo('setpos');
        },
        activeMobileSlider: function(){

            $(this.products).addClass('owl-carousel').owlCarousel(this.params);
            $(this.products).find('.owl-prev').after($(this.products).find('.owl-dots'));
        },
        disableMobileSlider: function(){
            $(this.products).removeClass('owl-carousel').trigger('destroy.owl.carousel');
        }
    }

    var mobileFilter = {
        wrapp: '.filters_block_wrapper',
        filterBlock: '.filters_block',
        openBtn: '.open_filter_btn',
        closeBtn: '.close_filter_btn',
        activMobile: false,

        init: function(){
            var _this = this;

            _this._activeDisableMobile();
            $(window).on('resize', function(){
                _this._activeDisableMobile();
            });
        },
        _activeDisableMobile: function(){
            if(window.innerWidth > 960){
                if(this.activMobile == true){
                    this.disableMobileActions();

                    this.activMobile = false;
                }
            }else{
                if(this.activMobile == false){
                    this.activeMobileActions();

                    this.activMobile = true;
                }
            }
        },
        activeMobileActions: function(){
            this._open();
            this._close();
        },
        disableMobileActions: function(){
            $(this.openBtn + ', ' + this.closeBtn).unbind('click');
            this._hide();
        },
        _scroll: function(action){
            var _this = this;

            if(action == false){
                $(_this.filterBlock).removeAttr('style');
            }else{
                $(window).bind('scroll', function(event){
                    if($(window).height() < $(_this.filterBlock).height()){
                        var top = window.pageYOffset || document.body.scrollTop || document.documentElement.scrollTop,
                            maxTop = $(_this.filterBlock).height() - $(window).height();
                        if(top < 0){
                            top = 0;
                        }else if(top > maxTop){
                            top = maxTop;
                        }
                        $(_this.filterBlock).css('top', '-' + top + 'px');
                    }
                });
            }
        },
        _open: function(){
            var _this = this;

            $(this.openBtn).bind('click', function(){
                _this._show(_this.wrapp);
            });
        },
        _close: function(){
            var _this = this;

            $(this.closeBtn).bind('click', function(){
                _this._hide(_this.wrapp);
            });

            $(document).on('click', function(event) {
                if (!$(event.target).closest(_this.filterBlock).length && !$(event.target).closest(_this.openBtn).length) {
                    _this._hide();
                }
                event.stopPropagation();
            });
        },
        _show: function(wrapp){
            var _this = this;

            $(wrapp).show();
            window.baseObj.fixBody($(wrapp));
            setTimeout(function(){
                $(wrapp).addClass('open');
                _this._scroll();
                //window.baseObj.scrollThis($(wrapp));
            }, 100);
        },
        _hide: function(wrapp){
            var _this = this;

            if(wrapp === undefined){
                wrapp = this.wrapp;

                $(wrapp).removeClass('open');
                setTimeout(function(){
                    $(wrapp).removeAttr('style');
                    _this._scroll(false);
                    window.baseObj.fixBody($(wrapp), false);
                }, 300);
            }else{
                $(wrapp).removeClass('open');
                setTimeout(function(){
                    $(wrapp).removeAttr('style');
                    _this._scroll(false);
                    window.baseObj.fixBody($(wrapp), false);
                }, 300);
            }
        }
    }

    $( document ).ready(function() {
        products.init();
        filters.init();
        mobileFilter.init();

        if (filters.ishistori_api) {
            window.onpopstate = function(ev) {
                if (ev.state) {
                    filters.clearParams();
                    filters.getSearchParams(ev.state.page);
                    filters.checkedSearchParams();
                    filters.filtering();
                }
            }
        }
    });

});

