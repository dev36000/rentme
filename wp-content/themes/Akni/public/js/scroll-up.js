requirejs([
    'jquery',
    'jquery-ui.min'
], function ($) {
    'use strict';
    var scroll_up = {
        scrollup:'#scrollup',

        init: function(){
            var _this = this;
            this._initScrollUp();
        },


        _initScrollUp: function() {
            var _this = this;

            $(this.scrollup).click(function () {
                $('body,html').animate({
                    scrollTop: 0
                }, 800);
            });

            $(document).scroll(function() {
                if ($(this).scrollTop() > 0) {
                    $(_this.scrollup).fadeIn();
                } else {
                    $(_this.scrollup).fadeOut();
                }
            });
        },
}


    $( document ).ready(function() {
        scroll_up.init();

    });

});


