!(function(){
var t=0, tm, rtm;
/**
 * 
 * <style>
 *  #bottommarker{}
 *  #bottomtext{left:-99999px;opacity:0;}
 *  .bottomwrap{height:1px;}
 * </style>
 * <div class="bottomwrap">
 *  <p id="bottomtext" >... </p> SEO текст
 * </div>
 * ...
 * <div class="bottommarker"></div> элемент для расчета позиционирования блока текста
 */

tm = setInterval(function(){
    if (t>100) {clearInterval(tm);} t++;
if (typeof window.jQuery === 'function') {
    clearInterval(tm);
jQuery.fn.extend({
    /**
     * marker - элемент для вставки блока текста скриптом
     * text   - элемент с SEO текстом
     * time   - задержка (40мс)
     * class  - класс для блока текста
     * ---
     */
    seo: function(options){
        var elmarker, eltext;
       
        jQuery(document).ready(function() {

            if (typeof options === 'object' || typeof options === 'undefined') {
                options = options || {};
                // класс блока текста для темы shaper_helix3 - "su-section-content su-content-wrap"
                //if (options.class) {jQuery("#bottomtext").addClass(options.class);}
            } 
            else {
                switch (options) {
                    case 'setpos':
                        create();
                    break;
                }
                return true;
            }
            create();
            jQuery(window).resize(create);
        });

        function create() {
            jQuery("#bottomtext").css({opacity: 0, '-khtml-opacity':0});
            clearTimeout(rtm);
            rtm = setTimeout(function(){fnsetpos();}, options.time || 200);
        }

        /**
         * получаем и устанавливаем позицию блока
         */
        function fnsetpos() {
            elmarker = jQuery("#bottommarker");
            eltext   = jQuery("#bottomtext");
            // устанавливаем высоту блока маркера
            elmarker.height(eltext.outerHeight());
            // позиционируем блок текста в место блока маркера
            eltext
                .offset({top : elmarker.offset().top})
                .css({left: 0, right: 0, opacity: 1, '-khtml-opacity':1, position: 'relative'});       
        }        
    }
});
}
}, 40); // setInterval
})();