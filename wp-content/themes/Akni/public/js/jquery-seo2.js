!(function(){
    var t=0, tm, rtm;
    tm = setInterval(function(){
        if (t>100) {clearInterval(tm);} t++;
    if (typeof window.jQuery === 'function') {
        clearInterval(tm);    
        /**
        * marker - элемент для вставки блока текста скриптом
        * text   - элемент с SEO текстом
        * time   - задержка (40мс)
        * class  - класс для блока текста
        * ---
        */
            var elmarker, eltext, isclone = false;
            jQuery(document).ready(function() {
                elmarker = jQuery("#bottommarker");
                eltext   = jQuery("#bottomtext").clone().css({left: 0, right: 0, opacity: 1, '-khtml-opacity':1, position: 'relative'}).attr('id', 'textwm');
                jQuery(window).on('scroll', scroll);
            });

            function scroll() {
                if (isclone) {jQuery(window).off('scroll', scroll); return false;}
                clearTimeout(rtm);
                rtm = setTimeout(clone, 150);
            }

            function clone() {
                elmarker = elmarker || jQuery("#bottommarker");
                if (elmarker.length && jQuery(window).scrollTop() + jQuery(window).height() + 200 >= elmarker.offset().top) {
                    isclone = true;
                    elmarker.html(eltext);
                }
            }

}
}, 40); // setInterval   
})();