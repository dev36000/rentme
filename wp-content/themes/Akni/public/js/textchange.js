jQuery(document).ready(function(){
	var elmarker, eltext;
	setTimeout(function() {
		// элемент для вставки блока текста скриптом
		elmarker = jQuery("#bottommarker");
		// элемент с SEO текстом
		eltext = jQuery("#bottomtext");
  
  	// класс блока текста для темы shaper_helix3
  	eltext.addClass("su-section-content su-content-wrap");
  
  	// устанавливаем высоту блока маркера
  	elmarker.height(eltext.outerHeight());
  	// позиционируем блок текста в место блока маркера
  	eltext
        .offset({top : elmarker.offset().top})
        .css({left: 0, right: 0, opacity: 1});
  }, 400);
});