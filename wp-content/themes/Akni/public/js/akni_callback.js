requirejs([
    'jquery',
    'jquery-ui.min'
], function ($) {
    'use strict';

(function($){
    class Validator {
        constructor(form, name, tel, email, counter, thankyou, dateFrom, dateTo, ajaxUrl) {
            this.counter = counter;
            this.form = form;
            this.name = name;
            this.tel  = tel;
            this.email= email;
            this.thankyou = thankyou;
            this.date_from = dateFrom;
            this.date_to = dateTo;
            this.ajaxUrl = ajaxUrl;
            this.init();
        }
        
        init(){
            this.phone_mask();
            this.name_mask();
            this.validate_change_action();
            this.validate_click_action();
        }
        
        is_valid_phone_number(phone_number) {
            phone_number = phone_number.substr(1);
            var regExpObj = /\d\d\s\(\d\d\d\)\s\d\d-\d\d-\d\d\d/;
            return !(regExpObj.exec(phone_number) == null);
        }

        is_valid_date(date) {
            var regExpObj = /(\d{2}).(\d{2}).(\d{4})/;
            //return date.test(regExpObj);
            //return false;
            return regExpObj.exec(date) != null;
        }
        
        is_valid_name(name) {
            return name.length > 1 && name.length < 40;
        }
        
        is_valid_email(email){
            var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,6})+$/;
            return re.test(email);
        }
        
        name_mask(){
            $(this.name).keydown(function(){
                if(/[^a-zA-Zа-яА-ЯёЁ`ґєҐЄ´ІіЇї .]/i.test(this.value)){
                    this.value = this.value.slice(0, -1)
                }
            });
            $(this.name).keyup(function(){
                if(/[^a-zA-Zа-яА-ЯёЁ`ґєҐЄ´ІіЇї .]/i.test(this.value)){
                    this.value = this.value.slice(0, -1)
                }
            });
        }
        
        phone_mask(){
            $(this.tel).phoneMask();
            //$(this.tel).mask("+38(999)-99-99-999")
        }
        
        validate_name(name){
            var _this = this;
            var errClass ='name-error-msg-'+this.counter;
            var errSelector = 'span.name-error-msg-'+this.counter;

            if (! _this.is_valid_name($(name).val())) {
                $(name).toggleClass('field-error', true);
                if (!$(errSelector).html()) {
                    $(name).after('<span class="'+errClass+'">' + $(name).attr('data-error') + '</span>');
                }
            } else {
                $(name).toggleClass('field-error', false);
                $(errSelector).fadeOut().remove();
            }
        }
        
        validate_tel(tel){
            var _this = this;
            var errClass ='tel-error-msg-'+this.counter;
            var errSelector = 'span.tel-error-msg-'+this.counter;

            if (! _this.is_valid_phone_number($(tel).val())) {
                $(tel).toggleClass('field-error', true);

                if (!$(errSelector).html()) {
                    $(tel).after('<span class="'+errClass+'">' + $(tel).attr('data-error')  + '</span>');
                }
            } else {
                $(tel).toggleClass('field-error', false);
                $(errSelector).fadeOut().remove();
            }
        }
        
        validate_email(email){
            var _this = this;
            var errClass ='email-error-msg-'+this.counter;
            var errSelector = 'span.email-error-msg-'+this.counter;

            if (! _this.is_valid_email($(email).val())) {
                $(email).toggleClass('field-error', true);

                if (!$(errSelector).html()) {
                    $(email).after('<span class="'+errClass+'">' + $(email).attr('data-error') + '</span>');
                }
            } else {
                $(email).toggleClass('field-error', false);
                $(errSelector).fadeOut().remove();
            }
        }
        
        validate_change_action(){
            var _this = this;

            $(_this.tel).change(function(){
                _this.validate_tel(this);
            });
            $(_this.name).change(function(){
                _this.validate_name(this);
            });
            $(_this.email).change(function(){
                _this.validate_email(this);
            });
            $(_this.date_from).change(function(){
                _this.validateDateFrom(this);
            });
            $(_this.date_to).change(function(){
                _this.validateDateTo(this);
            });
        }

        validateDateFrom(date_from) {
            var _this = this;
            var errClass ='date-from-error-msg-'+this.counter;
            var errSelector = 'span.date-from-error-msg-'+this.counter;

            if (! _this.is_valid_date($(date_from).val())) {
                $(date_from).toggleClass('field-error', true);

                if (!$(errSelector).html()) {
                    $(date_from).after('<span class="'+errClass+'">' + $(date_from).attr('data-error') + '</span>');
                }
            } else {
                $(date_from).toggleClass('field-error', false);
                $(errSelector).fadeOut().remove();
            }
        }
        validateDateTo(date_to) {
            var _this = this;
            var errClass ='date-to-error-msg-'+this.counter;
            var errSelector = 'span.date-to-error-msg-'+this.counter;

            if (! _this.is_valid_date($(date_to).val())) {
                $(date_to).toggleClass('field-error', true);

                if (!$(errSelector).html()) {
                    $(date_to).after('<span class="'+errClass+'">' + $(date_to).attr('data-error') + '</span>');
                }
            } else {
                $(date_to).toggleClass('field-error', false);
                $(errSelector).fadeOut().remove();
            }
        }
        
        sendForm(data) {
            var _this = this;

            $('.form_loader').show();
            $.ajax({
                type: 'POST',
                url: _this.ajaxUrl,
                data: {
                    'action':'sendCallback',
                    'data': data
                },
                success: function () {
                    $(_this.form).hide();
                    $('.form_loader').hide();
                    $(_this.thankyou).show();
                },
                error: function () {
                    console.log('this is error. Tell about it to me please (stevenaknidev@gmail.com).')
                }

            })
        }
        
        validate_click_action(){
            var _this = this;
            $(_this.form).submit(function(evt) {
                var valid = true;
                if ($(_this.tel).attr('name') != undefined) {
                    if (!_this.is_valid_phone_number($(_this.tel).val())) {
                        _this.validate_tel(_this.tel);
                        valid = false;
                    }
                }
                if ($(_this.name).attr('name') != undefined) {
                    if(! _this.is_valid_name($(_this.name).val())) {
                        _this.validate_name(_this.name);
                        valid = false;
                    }
                }
                if ($(_this.email).attr('name') != undefined) {
                    if(! _this.is_valid_email($(_this.email).val())) {
                        _this.validate_email(_this.email);
                        valid = false;
                    }
                }
                if ($(_this.date_from).attr('name') != undefined) {
                    if(! _this.is_valid_date($(_this.date_from).val())) {
                        _this.validateDateFrom(_this.date_from);
                        valid = false;
                    }
                }
                if ($(_this.date_to).attr('name') != undefined) {
                    if(! _this.is_valid_date($(_this.date_to).val())) {
                        _this.validateDateTo(_this.date_to);
                        valid = false;
                    }
                }
                if(!valid) {
                    return false;
                } else {
                    var from_val = $(_this.date_from).val() + ' ' + $(_this.date_from).attr('data-hours') + ':' + $(_this.date_from).attr('data-minutes');
                    var to_val = $(_this.date_to).val() + ' ' + $(_this.date_to).attr('data-hours') + ':' +$(_this.date_to).attr('data-minutes');

                    $(this).find('input[name="time_from"]').val(from_val);
                    $(this).find('input[name="time_to"]').val(to_val);
                    // $(this).find('input[name="time_from"]:not(.time_date)').val(from_val);
                    // $(this).find('input[name="time_to"]:not(.time_date)').val(to_val);
                }
                evt.preventDefault();
                if($(this).closest('.callback_form_block').length){
                    $(this).find('input[type="submit"]').hide();
                    $(this).find('.form_loader').css('display', 'inline-block');
                }


                _this.sendForm($(this).serialize());
            });
        }

    }
    
    $(document).ready(function () {
        $(".callback-form").each(function(i) {
            var currentId =   '#'+this.id;
            var ajaxUrl   =    $(currentId).data('url');
            var currentName = '#'+ $(currentId).find('.name').attr('id');
            var currentTel  = '#'+ $(currentId).find('.tel').attr('id');
            var currentEmail= '#'+ $(currentId).find('.email').attr('id');
            var thankYouText ='#'+ $(currentId).parent().find('.thank-you').attr('id');
            var dateFrom ='#'+ $(currentId).parent().find('.date_from').attr('id');
            var dateTo ='#'+ $(currentId).parent().find('.date_to').attr('id');
            new Validator(currentId,currentName,currentTel,currentEmail,i,thankYouText,dateFrom,dateTo,ajaxUrl);
        });
    });
})(jQuery);
});