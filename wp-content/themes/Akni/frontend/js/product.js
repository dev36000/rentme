requirejs([
    'jquery'
], function ($) {
    'use strict';
    (function( global, factory ) {
        "use strict";
        if ( typeof module === "object" && typeof module.exports === "object" ) {
            module.exports = global.document ?
                factory( global, true ) :
                function( w ) {
                    if ( !w.document ) {
                        throw new Error( "jQuery requires a window with a document" );
                    }
                    return factory( w );
                };
        } else {
            factory( global );
        }
    } )( typeof window !== "undefined" ? window : this, function( window, noGlobal ) {
        "use strict";

        (function($) {
            $.fn.myVerticalCarousel = function(params){
                var block = this;
                var params = params;

                var myCarousel = function(block){
                    this.params = params;
                    this.block = block;
                    //this.blockHeight = 0;
                    this.blockHeight = 452;
                    this.visibleHeight = 0;
                    this.carouselName = 'v_carousel';
                    this.bigImg = '.images_block .image';
                    this.items = '.galery_item';
                    this.scrollBy = 2;
                    this.visibleItems = null;
                    this.active = null;
                    this.arrowHeight = 40; //hardcode

                    this.init = function(){
                        var _this = this;

                        this.setHeight();
                        /*$(window).on('resize', function(){
                         _this.setHeight();
                         });*/
                        this.isActive();
                        $(window).on('resize', function(){
                            _this.isActive();
                        });
                    }

                    this.isActive = function(){
                        if(this.params == 'destroy'){
                            if(this.active == false || this.active == null){
                                this.disable();

                                this.active = true;
                            }
                        }else{
                            if(this.active == true || this.active == null){
                                this.activate();

                                this.active = false;
                            }
                        }
                    }

                    this.activate = function(){
                        var _this = this;

                        this.keyboardSlide();

                        this.makeWrapp(this.block);
                        $('.' + this.carouselName + '_up, .' + this.carouselName + '_down').bind('click', function(){
                            _this.slide($(this));
                        });

                        this.checkBigImg();
                    }

                    this.keyboardSlide = function(){
                        var _this = this;

                        $(document).on('click', function(event){
                            if($(event.target).closest('.' + _this.carouselName).length){
                                _this._unbindKeys();
                                _this._bindKeys();
                            }else{
                                _this._unbindKeys();
                            }
                        });
                    }

                    this._bindKeys = function(){
                        var _this = this;

                        $(document).bind('keydown', function(event){
                            if(event.keyCode == 38){
                                _this.keySlide('up');
                                event.preventDefault();
                            }
                            if(event.keyCode == 40){
                                _this.keySlide('down');
                                event.preventDefault();
                            }
                        });
                    }

                    this.keySlide = function(to){
                        var activeImgIndex = $(this.items + '.active').index(),
                            top = 0,
                            topIndex = $(this.items + '.top').index(),
                            visibleItems = this.visibleItems,
                            maxIndex = $(this.items).length - visibleItems,
                            maxDiff = $(this.items).outerHeight(true) * $(this.items).length - this.visibleHeight;

                        if(to == 'up'){
                            activeImgIndex--;
                        }else{
                            activeImgIndex++;
                        }

                        if(activeImgIndex < 0)
                            activeImgIndex = 0;
                        else if(activeImgIndex > ($(this.items).length - 1))
                            activeImgIndex = $(this.items).length - 1;

                        $(this.items).removeClass('active');
                        $(this.items).eq(activeImgIndex).addClass('active');

                        var bigUrl = $(this.items).eq(activeImgIndex).find('img').attr('src').replace('-' + $(this.items).eq(activeImgIndex).find('img').attr('src').split('-').pop().split('.')[0], '');
                        $(this.bigImg).find('img').attr('src', bigUrl);
                        $(window).resize();

                        if((visibleItems + topIndex) == activeImgIndex){
                            $(this.items).removeClass('top');
                            $(this.items).eq(topIndex + 1).addClass('top');
                        }else if(activeImgIndex < topIndex){
                            $(this.items).removeClass('top');
                            $(this.items).eq(topIndex - 1).addClass('top');
                        }



                        top = $(this.items + '.top').position().top;

                        $('.' + this.carouselName + '_inner').css('top', '-' + top + 'px');

                        topIndex = $(this.items + '.top').index();

                        if(topIndex != 0){
                            $('.' + this.carouselName + '_up').removeClass('disabled');
                        }
                        if(topIndex == ($(this.items).length - visibleItems - 1)){
                            $('.' + this.carouselName + '_down').removeClass('disabled');
                        }

                        if(topIndex == 0){
                            $('.' + this.carouselName + '_up').addClass('disabled');
                        }

                        if(topIndex == ($(this.items).length - visibleItems)){
                            $('.' + this.carouselName + '_down').addClass('disabled');
                        }
                    }

                    this._unbindKeys = function(){
                        $(document).unbind('keydown');
                    }

                    this.disable = function(){
                        $('.' + this.carouselName + '_up, .' + this.carouselName + '_down').unbind('click');
                        this.unwrapp(this.block);
                    }

                    this.slide = function(elem){
                        this._unbindKeys();
                        var to = elem.attr('class').split('_').pop().split(' ')[0],
                            top = 0,
                            topIndex = $(this.items + '.top').index(),
                            visibleItems = this.visibleItems,
                            maxIndex = $(this.items).length - visibleItems,
                            maxDiff = $(this.items).outerHeight(true) * $(this.items).length - this.visibleHeight;

                        switch(to){
                            case 'up':
                                topIndex -= this.scrollBy;
                                break;
                            case 'down':
                                topIndex += this.scrollBy;
                                break;
                        }

                        if(topIndex < 0)
                            topIndex = 0;
                        else if(topIndex > maxIndex)
                            topIndex = maxIndex;

                        $('.' + this.carouselName + '_up, .' + this.carouselName + '_down').removeClass('disabled');
                        if(topIndex == 0 || topIndex == maxIndex)
                            elem.addClass('disabled');

                        $(this.items).removeClass('top').eq(topIndex).addClass('top');
                        top = $(this.items + '.top').position().top;
                        if(top > maxDiff)
                            top = maxDiff;

                        $('.' + this.carouselName + '_inner').css('top', '-' + top + 'px');
                    }

                    this.checkBigImg = function(){
                        var _this = this;

                        $(this.items).on('click', function(){
                            if(!$(this).hasClass('active')){
                                $(_this.items).removeClass('active');
                                $(this).addClass('active');

                                var bigUrl = $(this).find('img').attr('src').replace('-' + $(this).find('img').attr('src').split('-').pop().split('.')[0], '');
                                $(_this.bigImg).find('img').attr('src', bigUrl);
                                $(window).resize();
                            }
                        });
                    }

                    this.setHeight = function(){
                        //this.blockHeight = $(this.bigImg).height();
                        //this.blockHeight = $('.' + this.carouselName).height();

                        this.visibleHeight = this.blockHeight - (this.arrowHeight * 2);
                        this.visibleItems = Math.floor(this.visibleHeight / $(this.items).outerHeight(true));

                        if($('.' + this.carouselName).length){
                            $('.' + this.carouselName).css('height', this.blockHeight + 'px');
                        }
                    }

                    this.makeWrapp = function(obj){
                        obj.addClass(this.carouselName).attr('tabindex', 1).css('height', this.blockHeight + 'px');

                        if(!obj.find('.' + this.carouselName + '_wrapp').length)
                            obj.wrapInner('<div class="' + this.carouselName + '_wrapp"><div class="' + this.carouselName + '_inner"></div></div>').prepend('<span class="' + this.carouselName + '_up disabled"></span>').append('<span class="' + this.carouselName + '_down"></span>').find(this.items).eq(0).addClass('active top');
                    }

                    this.unwrapp = function(obj){
                        if(obj.find('.' + this.carouselName + '_wrapp').length){
                            obj.removeClass(this.carouselName).removeAttr('style').find('.' + this.carouselName + '_up, .' + this.carouselName + '_down').remove();
                            $(this.items).unwrap().unwrap();
                        }
                    }

                }

                var carousel = new myCarousel(block);
                carousel.init();
            }
        })(jQuery);

        return jQuery;
    } );
    var productImages = {
        wrapp: '.images_block',
        img: '.image img',
        galeryWrapp: '.galery',
        galeryItem: '.galery_item',
        isMobile: null,
        isRemake: false,
        params: {
            items: 1,
            nav: true,
            loop: false,
            autoHeight: true
        },
        init: function(){
            var _this = this;


            _this._activeDisableMobile();
            $(window).on('resize', function(){
                _this._activeDisableMobile();
            });
        },
        _activeDisableMobile: function(){
            if(window.innerWidth > 640){
                if(this.isMobile == true || this.isMobile == null){
                    this._active();

                    this.isMobile = false;
                }
            }else{
                if(this.isMobile == false || this.isMobile == null){
                    this._disable();

                    this.isMobile = true;
                }
            }
        },
        _active: function(){
           // console.log('_active');
            this.makeThumbs(false);
            $(this.wrapp + ' ' + this.galeryWrapp).removeClass('owl-carousel').trigger('destroy.owl.carousel');
            $(this.wrapp + ' ' + this.galeryWrapp).myVerticalCarousel();
        },
        _disable: function(){
            this.makeThumbs();
            $(this.wrapp + ' ' + this.galeryWrapp).myVerticalCarousel('destroy');
            $(this.wrapp + ' ' + this.galeryWrapp).addClass('owl-carousel').owlCarousel(this.params);
            $(this.wrapp).find('.owl-prev').after($(this.wrapp).find('.owl-dots'));
        },
        makeThumbs: function(action){
            var _this = this;

            if(action == false){
                if(this.isRemake){
                    $(this.galeryItem + ' img').each(function(){
                        $(this).attr('src', $(this).attr('data-min-url'));
                    });
                }
            }else{
                $(this.galeryItem + ' img').each(function(){
                    $(this).attr('src', $(this).attr('data-big-url'));
                });
                this.isRemake = true;
            }
        }
    }

    var productTabs = {
        wrapp: '.tabs_block',
        head: '.tabs_head',
        body: '.tabs_body',

        init: function(){
            this._showTab();
            this._onClick();
        },
        _onClick: function(){
            var _this = this;

            $(this.head + ' span').on('click', function(){
                if(!$(this).hasClass('active')){
                    _this._showTab($(this).index());
                }
            });
        },
        _showTab: function(index){
            if(index === undefined)
                index = 0;

            $(this.head + ' span').removeClass('active');
            $(this.head + ' span').eq(index).addClass('active');
            $(this.body + '>div').removeClass('active');
            $(this.body + '>div').eq(index).addClass('active');
        }
 
    }

    var similarProducts = {
        wrapp: '.product_page .deals_block',
        params: {
            margin: 10,
            nav: true,
            loop: false,
            autoHeight: true,

            responsive: {
                0: {
                    items: 1,
                    margin: 0
                },
                640: {
                    items: 2,
                    margin: 0
                },
                800: {
                    items: 3
                },
                1000: {
                    items: 4
                }
            }
        },

        init: function(){
            $(this.wrapp).owlCarousel(this.params);
            $(this.wrapp).find('.owl-prev').after($(this.wrapp).find('.owl-dots'));
            $('.best-deals .deal .image-holder .image').setSameHeight();
            $('.best-deals .deal .title').setSameHeight();
        }
    };


    $( document ).ready(function() {
        productImages.init();
        productTabs.init();
        similarProducts.init();
    });
});