requirejs([
    'jquery',
    'jquery-ui.min'
], function ($) {
    'use strict';
    var filters = {
        hash_params : {},
        cars:'#products_list_block',
        sliderRange: '#slider-range',
        amount: '#amount',
        resultFrom: '.result_from',
        resultTo: '.result_to',
        resetBtn: '.reset_filters',

        init: function ()
        {
            this.sliderInit();
            this.getHashParams();
            this.filtering();
            this.checkedParams();
            this.changeFilter();
            this.filterReset();
        },

        filterReset: function()
        {
            var _this = this;

            $(this.resetBtn).on('click', function(){
                _this.hash_params = {}
                _this.setHash();
                $('.filters input').prop("checked", false);
                _this.sliderInit();
                _this.filtering();
                $('.filters .sel li.def').click();
            });
        },

        sliderInit :function()
        {
            var _this = this;

            var thisSearch = $('input.price');

            $(_this.resultFrom + ' .value input, ' + _this.resultTo + ' .value input').on('change', function(){
                var s = $( _this.sliderRange).slider();
                s.slider('values',[$(_this.resultFrom + ' .value input').val(), $(_this.resultTo + ' .value input').val()]);
                s.trigger('slide',{ ui: $('.ui-slider-handle', s), value: 10 });

                var thisVal = $(_this.resultFrom + ' .value input').val() + ',' + $(_this.resultTo + ' .value input').val();
                thisSearch.val(thisVal);
                //_this.addHash($(  _this.amount ), thisVal);
                //_this.setHash();
                // _this.addSearch($(this));
                _this.addSearch(thisSearch,thisVal);
                _this.filtering();
                console.log(_this);
                $('#show-more-filters').click();
            });

            $( _this.sliderRange).slider({
                range: true,
                min: $(_this.amount).data('min'),
                max: $(_this.amount).data('max'),
                values: [ $(_this.amount).data('min'), $(_this.amount).data('max') ],
                slide: function( event, ui ) {
                    $( _this.amount).val(  window.baseObj.currency + ui.values[0] +
                        " - " +
                        window.baseObj.currency + ui.values[1] );

                    $(_this.resultFrom + ' .value .currency').html(window.baseObj.currency);
                    $(_this.resultFrom + ' .value input').val(ui.values[0]);

                    $(_this.resultTo + ' .value .currency').html(window.baseObj.currency);
                    $(_this.resultTo + ' .value input').val(ui.values[1]);

                },

                stop: function( event, ui ) {

                    var thisVal = ui.values[0] + ',' + ui.values[1];
                    thisSearch.val(thisVal);
                    // _this.addHash($(  _this.amount ), thisVal);
                    //  _this.setHash();
                    _this.addSearch(thisSearch,thisVal);
                    _this.filtering();
                    $('#show-more-filters').click();
                }
            });

            $(_this.amount).val(
                window.baseObj.currency + $( _this.sliderRange).slider( "values", 0 ) +
                " - " +
                window.baseObj.currency + $( _this.sliderRange).slider( "values", 1 )
            );
            $(this.resultFrom + ' .value .currency').html(window.baseObj.currency);
            $(this.resultFrom + ' .value input').val($( _this.sliderRange).slider( "values", 0 ));

            $(this.resultTo + ' .value .currency').html(window.baseObj.currency);
            $(this.resultTo + ' .value input').val($( _this.sliderRange).slider( "values", 1 ));
        },


        getHashParams : function () {
            var _this = this;
            var hash_array = location.hash.substring(1).split('&');
            var hash_key_val = new Array(hash_array.length);

            for (var i = 0; i < hash_array.length; i++) {
                hash_key_val[i] = hash_array[i].split('=');
            }

            if (hash_key_val.length > 0 ) {
                for (var i = 0; i < hash_key_val.length; i++) {
                    var hash = hash_key_val[i];
                    _this.hash_params[hash[0]] = hash;

                }
            }
        },

        filtering :function () {
            var _this = this;

            var formData = {
                'action':'carFiltering',
                'data': _this.hash_params,
            };

            window.baseObj.ajaxAction(formData, this._success);
        },

        checkedParams: function () {
            var _this = this;
            for (var elem in _this.hash_params) {
                if(_this.hash_params[elem][1] != undefined && elem != 'price') {
                    var values = _this.hash_params[elem][1].split(',');
                   //console.log(values);

                    for(var i = 0; i < values.length; i++){
                        $('.filters input[name="' + elem + '"][value="' + values[i] + '"]').attr('checked', 'checked');
                    }
                }
            }
        },

        addHash : function(thisInput , slider) {
            var _this = this;

            var str = '';
            $('.filters input[name="' + thisInput.attr('name') + '"]:checked').each(function(i){
                if(i != 0)
                    str += ',';

                str += $(this).val();
            });

            if (slider !== undefined) {

                str = slider;
            }

            var attrName = _this.hash_params[thisInput.attr('name')];

            if (attrName) {
                attrName[1] = str;
            } else{
                _this.hash_params[thisInput.attr('name')] = [thisInput.attr('name'), str];
            }
        },

        setHash: function()
        {
            var hash = '#', i = 0, _this = this;
            for(var filter in _this.hash_params){
                var name = _this.hash_params[filter][0];
                var values = _this.hash_params[filter][1];

                if (name == undefined || name =='' || values  == undefined || values =='') {
                    continue;
                }

                if(i != 0) {
                    hash += '&';
                }
                hash += name + '=' + values;

                i++;
            }
            location.hash = hash;
        },

        changeFilter: function ()
        {
            var _this = this;

            $('.filters input').change(function(){
                _this.addHash($(this));
                _this.setHash();
                _this.filtering();
            });


        },

        _success: function(success)
        {
            if (success !="") {
                success = JSON.parse(success);
                if (success['content']) {
                    $(filters.cars).html(success['content']);
                    window.products.init();
                }
            }
        }
    };

    window.products = {
        wrapp: '.products_list_block',
        item: '.products_item',
        products: '.products_list_block .products_list',
        moreLink: 'more_cars_link',
        countOfVisible: 9,
        isMobile: null,
        params: {
            margin: 10,
            nav: true,
            pagination: true,
            loop: false,
            autoHeight: true,
            items: 1
        },

        init: function(){
            var _this = this;

            this.isMobile = null;
            $('.products_list .products_item .image-holder .image').setSameHeight();
            $('.products_list_block .products_item .price').setSameHeight();

            _this._activeDisableMobile();
            $(window).on('resize', function(){
                _this._activeDisableMobile();
            });
        },
        _activeDisableMobile: function(){
            if(window.innerWidth > 640){
                if(this.isMobile == true || this.isMobile == null){
                    this._disable();

                    this.isMobile = false;
                }
            }else{
                if(this.isMobile == false || this.isMobile == null){
                    this._active();

                    this.isMobile = true;
                }
            }
        },
        _active: function(){
            var _this = this;

            this.disableMobileSlider();

            if($(this.item).length > this.countOfVisible){
                $(this.wrapp).append('<span class="' + this.moreLink + '">'+baseObj.more_cars_text +'</span>');
            }

            _this.hideProducts();

            $('.' + this.moreLink).bind('click', function(){
                _this.showProducts();
            });
        },
        _disable: function(){
            this.hideProducts('all');
            $('.' + this.moreLink).remove();
            this.activeMobileSlider();
        },
        hideProducts: function(all){
            //console.log('hideProducts');
            var _this = this;

            if(all !== undefined){
                $(this.wrapp + ' ' + this.item).removeClass('hide');
                return;
            }

            $(this.wrapp + ' ' + this.item).each(function(i){
                if(i >= _this.countOfVisible){
                    $(this).addClass('hide');
                }
            });
        },
        showProducts: function(){
            var _this = this;

            $(this.wrapp + ' ' + this.item + '.hide').each(function(i){
                if(i < _this.countOfVisible){
                    $(this).removeClass('hide');
                }
            });

            if($(this.wrapp + ' ' + this.item + '.hide').length == 0)
                $('.' + this.moreLink).remove();
        },
        activeMobileSlider: function(){
            $(this.products).addClass('owl-carousel').owlCarousel(this.params);
            $(this.products).find('.owl-prev').after($(this.products).find('.owl-dots'));
        },
        disableMobileSlider: function(){
            $(this.products).removeClass('owl-carousel').trigger('destroy.owl.carousel');
        }
    }

    var mobileFilter = {
        wrapp: '.filters_block_wrapper',
        filterBlock: '.filters_block',
        openBtn: '.open_filter_btn',
        closeBtn: '.close_filter_btn',
        activMobile: false,

        init: function(){
            var _this = this;

            _this._activeDisableMobile();
            $(window).on('resize', function(){
                _this._activeDisableMobile();
            });
        },
        _activeDisableMobile: function(){
            if(window.innerWidth > 960){
                if(this.activMobile == true){
                    this.disableMobileActions();

                    this.activMobile = false;
                }
            }else{
                if(this.activMobile == false){
                    this.activeMobileActions();

                    this.activMobile = true;
                }
            }
        },
        activeMobileActions: function(){
            this._open();
            this._close();
        },
        disableMobileActions: function(){
            $(this.openBtn + ', ' + this.closeBtn).unbind('click');
            this._hide();
        },
        _scroll: function(action){
            var _this = this;

            if(action == false){
                $(_this.filterBlock).removeAttr('style');
            }else{
                $(window).bind('scroll', function(event){
                    if($(window).height() < $(_this.filterBlock).height()){
                        var top = window.pageYOffset || document.body.scrollTop || document.documentElement.scrollTop,
                            maxTop = $(_this.filterBlock).height() - $(window).height();
                        if(top < 0){
                            top = 0;
                        }else if(top > maxTop){
                            top = maxTop;
                        }
                        $(_this.filterBlock).css('top', '-' + top + 'px');
                    }
                });
            }
        },
        _open: function(){
            var _this = this;

            $(this.openBtn).bind('click', function(){
                _this._show(_this.wrapp);
            });
        },
        _close: function(){
            var _this = this;

            $(this.closeBtn).bind('click', function(){
                _this._hide(_this.wrapp);
            });

            $(document).on('click', function(event) {
                if (!$(event.target).closest(_this.filterBlock).length && !$(event.target).closest(_this.openBtn).length) {
                    _this._hide();
                }
                event.stopPropagation();
            });
        },
        _show: function(wrapp){
            var _this = this;

            $(wrapp).show();
            window.baseObj.fixBody($(wrapp));
            setTimeout(function(){
                $(wrapp).addClass('open');
                _this._scroll();
                //window.baseObj.scrollThis($(wrapp));
            }, 100);
        },
        _hide: function(wrapp){
            var _this = this;

            if(wrapp === undefined){
                wrapp = this.wrapp;

                $(wrapp).removeClass('open');
                setTimeout(function(){
                    $(wrapp).removeAttr('style');
                    _this._scroll(false);
                }, 300);
            }else{
                $(wrapp).removeClass('open');
                setTimeout(function(){
                    $(wrapp).removeAttr('style');
                    _this._scroll(false);
                    window.baseObj.fixBody($(wrapp), false);
                }, 300);
            }
        }
    }

    $( document ).ready(function() {
        products.init();
        filters.init();
        mobileFilter.init();
    });
});

